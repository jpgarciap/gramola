package es.udc.fic.tfg.jp.service.song.comparators;

import java.util.Comparator;

import es.udc.fic.tfg.jp.service.song.SongDto;

public class SongDtoLikesComparator implements Comparator<SongDto> {

	@Override
	public int compare(SongDto song1, SongDto song2) {
		return (int) (song2.getLikes() - song1.getLikes());
	}

}
