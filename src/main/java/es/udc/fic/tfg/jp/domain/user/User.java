package es.udc.fic.tfg.jp.domain.user;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.udc.fic.tfg.jp.domain.dislikedsong.DislikedSong;
import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylist;
import es.udc.fic.tfg.jp.domain.likedsong.LikedSong;
import es.udc.fic.tfg.jp.domain.role.Role;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "userId")
	private long userId;
	@Column(unique = true)
	private String email;
	private String password;
	private String name;
	private String surnames;
	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	private EditablePlaylist editablePlaylist;
	@OneToMany(mappedBy = "user")
	private Set<LikedSong> likes;
	@OneToMany(mappedBy = "user")
	private Set<DislikedSong> dislikes;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "user_role", joinColumns = { @JoinColumn(name = "userId") }, inverseJoinColumns = {
			@JoinColumn(name = "roleId") })
	private Set<Role> roles = new HashSet<Role>();

	public User() {
	}

	public User(String email, String password, String name, String surnames) {
		super();
		this.email = email;
		this.password = password;
		this.name = name;
		this.surnames = surnames;
		this.likes = new HashSet<LikedSong>();
		this.dislikes = new HashSet<DislikedSong>();
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurnames() {
		return surnames;
	}

	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}

	public Set<LikedSong> getLikes() {
		return likes;
	}

	public void setLikes(Set<LikedSong> likes) {
		this.likes = likes;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public EditablePlaylist getEditablePlaylist() {
		return editablePlaylist;
	}

	public void setEditablePlaylist(EditablePlaylist editablePlaylist) {
		this.editablePlaylist = editablePlaylist;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<DislikedSong> getDislikes() {
		return dislikes;
	}

	public void setDislikes(Set<DislikedSong> dislikes) {
		this.dislikes = dislikes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((likes == null) ? 0 : likes.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((roles == null) ? 0 : roles.hashCode());
		result = prime * result + ((surnames == null) ? 0 : surnames.hashCode());
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (likes == null) {
			if (other.likes != null)
				return false;
		} else if (!likes.equals(other.likes))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (roles == null) {
			if (other.roles != null)
				return false;
		} else if (!roles.equals(other.roles))
			return false;
		if (surnames == null) {
			if (other.surnames != null)
				return false;
		} else if (!surnames.equals(other.surnames))
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}

}
