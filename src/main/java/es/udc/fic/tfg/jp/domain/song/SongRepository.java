package es.udc.fic.tfg.jp.domain.song;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface SongRepository extends JpaRepository<Song, Long>, SongRepositoryCustom {

	Song findByName(String name);

	@Query("select count(s) > 0 from Song s where s.path = :path")
	boolean existsByPath(@Param("path") String path);

	@Query("select count(s) > 0 from Song s where s.title = :title and s.singers = :singers")
	boolean existsByTitleAndSingers(@Param("title") String title, @Param("singers") String singers);

	@Query("select s from Song s where s.genre.genreId = :genreId order by s.title")
	List<Song> findByGenre(@Param("genreId") long genreId);

	Song findByPath(String path);

	@Query("SELECT max(s.count) FROM Song s")
	long getMaxCount();

	@Modifying
	@Transactional
	@Query("delete from Song s where s.count = :count")
	void deleteByCount(@Param("count") long count);

}
