package es.udc.fic.tfg.jp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;

import javax.annotation.PostConstruct;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylist;
import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylistRepository;
import es.udc.fic.tfg.jp.domain.genre.Genre;
import es.udc.fic.tfg.jp.domain.genre.GenreRepository;
import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;
import es.udc.fic.tfg.jp.domain.role.Role;
import es.udc.fic.tfg.jp.domain.role.RoleRepository;
import es.udc.fic.tfg.jp.domain.song.Song;
import es.udc.fic.tfg.jp.domain.song.SongRepository;
import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.domain.user.UserRepository;

@Component
@Profile("production")
public class Initializer {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private GenreRepository genreRepository;
	@Autowired
	private SongRepository songRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private EditablePlaylistRepository editablePlaylistRepository;

	@Value("${song.path}")
	private String songPath;

	@PostConstruct
	private void init() throws IOException, SAXException, TikaException {
		createAdmin();
		Path path = Paths.get(songPath);
		long count;
		if (songRepository.findAll().size() == 0) {
			count = 0;
		} else {
			count = songRepository.getMaxCount();
		}

		fillDBSongs(path, count);
		// borramos las canciones de nuestra bd que ya no existen en el repositorio
		// local
		songRepository.deleteByCount(count);
	}

	private void createAdmin() {
		User userResult = userRepository.findByEmail("admin@udc.es");
		if (userResult == null) {
			User user = new User("admin@udc.es", "admin", "JP", "Garcia");
			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			Role userRole = roleRepository.findByName("USER_ROLE");
			Role adminRole = roleRepository.findByName("ADMIN_ROLE");
			HashSet<Role> roles = new HashSet<Role>();
			roles.add(userRole);
			roles.add(adminRole);
			user.setRoles(roles);
			userResult = userRepository.save(user);
			EditablePlaylist editablePlaylist = new EditablePlaylist(userResult, (long) 0, new ArrayList<OrderSong>());
			editablePlaylistRepository.save(editablePlaylist);

		}

	}

	// Lee las canciones de una carpeta local e introduce sus metados en nuestra BD
	// song y gender
	private void fillDBSongs(Path folderPath, long count) throws IOException, SAXException, TikaException {
		File folder = new File(folderPath.toString());
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory())
				fillDBSongs(fileEntry.toPath(), count);
			else
				readSongMetadata(fileEntry.toPath(), count);
		}
	}

	private void readSongMetadata(Path filePath, long count) throws IOException, SAXException, TikaException {

		String titleMetadata;
		String genreMetadata;
		String durationMetadata;
		String singersMetadata;
		String fileName;

		FileInputStream input = new FileInputStream(new File(filePath.toString()));
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		ParseContext pcontext = new ParseContext();
		Mp3Parser parser = new Mp3Parser();
		parser.parse(input, handler, metadata, pcontext);

		genreMetadata = metadata.get("xmpDM:genre").toLowerCase();
		Genre genre = genreRepository.findByName(genreMetadata);
		if (genre == null) {
			genre = new Genre(genreMetadata);
			genreRepository.save(genre);
		}
		titleMetadata = metadata.get("title").toLowerCase();
		singersMetadata = metadata.get("xmpDM:artist").toLowerCase();
		durationMetadata = metadata.get("xmpDM:duration").toLowerCase();

		long duration = (long) Double.parseDouble(durationMetadata);
		fileName = filePath.getFileName().toString();
		String path = filePath.toString().replaceAll("\\\\", "/");
		path = path.replace(songPath, "");
		// Buscamos por path, porque así las canciones pueden tener varias versiones
		// (mismo titulo y autor) y el path es unico
		if (!songRepository.existsByPath(path)) {
			// en el path le quitamos la parte del directorio principal
			Song song = new Song(fileName, genre, Files.probeContentType(filePath), duration, titleMetadata, path,
					singersMetadata, count + 1);
			songRepository.save(song);
		} else {
			Song song1 = songRepository.findByPath(path);
			song1.setCount(count + 1);
			songRepository.save(song1);
		}

	}

}
