
package es.udc.fic.tfg.jp.web.song;

import org.joda.time.LocalDate;

public class FindSongForm {

	private String keywords;
	private LocalDate date;
	private String orderBy;
	private long genreId;
	private boolean asc;

	public FindSongForm(String keywords, LocalDate date, String orderBy, long genreId, boolean asc) {
		super();
		this.keywords = keywords;
		this.date = date;
		this.orderBy = orderBy;
		this.genreId = genreId;
		this.asc = asc;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public long getGenreId() {
		return genreId;
	}

	public void setGenreId(long genreId) {
		this.genreId = genreId;
	}

	public boolean isAsc() {
		return asc;
	}

	public void setAsc(boolean asc) {
		this.asc = asc;
	}

}
