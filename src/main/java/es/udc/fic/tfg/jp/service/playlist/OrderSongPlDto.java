package es.udc.fic.tfg.jp.service.playlist;

import java.text.SimpleDateFormat;

import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;

public class OrderSongPlDto {

	private long orderSongId;
	private int position;
	private String title;
	private String singers;
	private String genreName;
	private long likes;
	private String duration;
	private String lastReproduction;
	private String path;

	public OrderSongPlDto() {

	}

	public OrderSongPlDto(OrderSong orderSong, long likes, String duration) {
		super();
		this.orderSongId = orderSong.getOrderSongId();
		this.position = orderSong.getPosition();
		this.title = orderSong.getSong().getTitle();
		this.singers = orderSong.getSong().getSingers();
		this.likes = likes;
		this.duration = duration;
		this.genreName = orderSong.getSong().getGenre().getName();
		if (orderSong.getSong().getLastReproduction() == null) {
			this.lastReproduction = "";
		} else {
			SimpleDateFormat localDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String strDate = localDateFormat.format(orderSong.getSong().getLastReproduction());
			this.lastReproduction = strDate;
		}

		this.path = orderSong.getSong().getPath();
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSingers() {
		return singers;
	}

	public void setSingers(String singers) {
		this.singers = singers;
	}

	public String getGenreName() {
		return genreName;
	}

	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}

	public String getLastReproduction() {
		return lastReproduction;
	}

	public void setLastReproduction(String lastReproduction) {
		this.lastReproduction = lastReproduction;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getLikes() {
		return likes;
	}

	public void setLikes(long likes) {
		this.likes = likes;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public long getOrderSongId() {
		return orderSongId;
	}

	public void setOrderSongId(long orderSongId) {
		this.orderSongId = orderSongId;
	}

}
