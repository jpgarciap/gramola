package es.udc.fic.tfg.jp.service.song.comparators;

import java.util.Comparator;

import es.udc.fic.tfg.jp.service.song.SongDto;

public class SongDtoDateComparator implements Comparator<SongDto> {

	@Override
	public int compare(SongDto song1, SongDto song2) {
		if (song1.getSong().getLastReproduction().after(song2.getSong().getLastReproduction()))
			return 1;
		else
			return 0;
	}

}
