package es.udc.fic.tfg.jp.service.user;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import es.udc.fic.tfg.jp.domain.role.Role;
import es.udc.fic.tfg.jp.domain.role.RoleRepository;
import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.domain.user.UserRepository;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public User registerUser(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		Role role = roleRepository.findByName("USER_ROLE");
		HashSet<Role> roles = new HashSet<Role>();
		roles.add(role);
		user.setRoles(roles);
		return userRepository.save(user);
	}

	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(email);
		if (user == null)
			throw new UsernameNotFoundException("User not found");
		return createUser(user);
	}

	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	public void signin(User user) {
		SecurityContextHolder.getContext().setAuthentication(authenticate(user));
	}

	private Authentication authenticate(User user) {
		return new UsernamePasswordAuthenticationToken(createUser(user), null, createAuthority(user));
	}

	private org.springframework.security.core.userdetails.User createUser(User user) {
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				createAuthority(user));
	}

	private List<GrantedAuthority> createAuthority(User user) {
		List<GrantedAuthority> listAuthority = new ArrayList<>();

		for (Role role : user.getRoles())
			listAuthority.add(new SimpleGrantedAuthority(role.getName()));

		return listAuthority;
	}
}
