package es.udc.fic.tfg.jp.service.song.comparators;

import java.util.Comparator;
import java.util.List;

import es.udc.fic.tfg.jp.service.song.SongDto;

public class SongDtoChainedComparator implements Comparator<SongDto> {

	private List<Comparator<SongDto>> listComparators;

	public SongDtoChainedComparator(List<Comparator<SongDto>> comparators) {
		this.listComparators = comparators;
	}

	@Override
	public int compare(SongDto song1, SongDto song2) {
		for (Comparator<SongDto> comparator : listComparators) {
			int result = comparator.compare(song1, song2);
			if (result != 0)
				return result;
		}
		return 0;
	}

}
