package es.udc.fic.tfg.jp.domain.editableplaylist;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;
import es.udc.fic.tfg.jp.domain.user.User;

@Entity
@Table(name = "editablePlaylist")
public class EditablePlaylist {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long editablePlaylistId;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;
	private long duration;
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private List<OrderSong> orderSongs = new ArrayList<>();

	public EditablePlaylist() {
	}

	public EditablePlaylist(User user, long duration, List<OrderSong> orderSongs) {
		super();
		this.user = user;
		this.duration = duration;
		this.orderSongs = orderSongs;
	}

	public long getEditablePlaylistId() {
		return editablePlaylistId;
	}

	public void setEditablePlaylistId(long editablePlaylistId) {
		this.editablePlaylistId = editablePlaylistId;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public User getUserId() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<OrderSong> getOrderSongs() {
		return orderSongs;
	}

	public void setOrderSongs(List<OrderSong> orderSongs) {
		this.orderSongs = orderSongs;
	}

	public void addDuration(long duration) {
		this.duration = this.duration + duration;
	}

	public void subtractDuration(long duration) {
		this.duration = this.duration - duration;
	}

}
