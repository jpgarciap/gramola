package es.udc.fic.tfg.jp.domain.event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.tfg.jp.domain.playlist.Playlist;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

	@Modifying
	@Transactional
	@Query("delete from Event e where e.playlist = :playlist")
	void deleteEventsByPlaylistId(@Param("playlist") Playlist playlist);

	Event findByName(String name);

}
