package es.udc.fic.tfg.jp.domain.genre;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {
	Genre findByName(String name);

	@Query("select g from Genre g order by g.name")
	List<Genre> findAllOrderByName();

}
