package es.udc.fic.tfg.jp.web.song;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import es.udc.fic.tfg.jp.domain.genre.Genre;
import es.udc.fic.tfg.jp.service.playlist.PlaylistService;
import es.udc.fic.tfg.jp.service.song.SongDto;
import es.udc.fic.tfg.jp.service.song.SongService;
import es.udc.fic.tfg.jp.service.user.UserService;
import es.udc.fic.tfg.jp.web.support.Pageable;

@Controller
public class SongController {

	@Autowired
	SongService songService;

	@Autowired
	PlaylistService playlistService;

	@Autowired
	UserService userService;

	String songDataUrl = "/admin/songsData?";

	@GetMapping("/admin/songs")
	public String findSongs(Model model, @RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "") String keywords, @RequestParam(defaultValue = "-1") long genreId,
			@RequestParam(required = false, value = "date", name = "date") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate date,
			@RequestParam(defaultValue = "Title") String orderBy, @RequestParam(defaultValue = "false") boolean asc) {

		// Para el desplegable de generos
		List<Genre> genreList = new ArrayList<>();
		Genre genre = new Genre("All");
		genre.setGenreId((long) -1);
		genreList.add(genre);
		genreList.addAll(songService.getAllGenreOrderByName());

		// Para el desplegable de ordenación
		List<String> orderByList = new ArrayList<>();
		orderByList.add("Title");
		orderByList.add("Singers");
		orderByList.add("Likes");
		orderByList.add("Last Reproduction");

		if (date == null) {
			date = LocalDate.now().minusMonths(1);
		}
		FindSongForm filters = new FindSongForm(keywords, date, orderBy, genreId, asc);

		Pageable<SongDto> songDtoPage = songService.findSongs(filters, PageRequest.of(page, 10));

		model.addAttribute("filters", filters);
		model.addAttribute("genreList", genreList);
		model.addAttribute("orderByList", orderByList);
		model.addAttribute("dateForDatePicker", date.toDate());
		model.addAttribute("data", songDtoPage);
		model.addAttribute("hasNext", hasNext(songDtoPage.getPage(), songDtoPage.getMaxPages()));
		model.addAttribute("hasPrevious", hasPrevious(songDtoPage.getPage(), songDtoPage.getMaxPages()));

		return "/admin/songs";
	}

	private int hasNext(int actualPage, int totalPage) {
		if (actualPage < totalPage)
			return 1;
		else
			return 0;

	}

	private int hasPrevious(int actualPage, int totalPage) {
		if (actualPage > 1 && actualPage <= totalPage)
			return 1;
		else
			return 0;
	}

}
