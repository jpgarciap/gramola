package es.udc.fic.tfg.jp.domain.song;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;

import es.udc.fic.tfg.jp.domain.genre.Genre;
import es.udc.fic.tfg.jp.domain.genre.GenreRepository;

public class SongRepositoryCustomImpl implements SongRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	GenreRepository genreRepository;

	@Override
	public List<Song> findByKeywords(Set<String> keywords) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Song> q = cb.createQuery(Song.class);
		Root<Song> song = q.from(Song.class);
		List<Predicate> predicates = new ArrayList<>();
		for (String word : keywords) {
			predicates.add(cb.or(cb.like(song.get("title"), "%" + word + "%"),
					cb.like(song.get("singers"), "%" + word + "%")));

		}
		List<Predicate> or = new ArrayList<>();
		for (Predicate p : predicates) {
			or.add(cb.or(p));
		}

		q.select(song).distinct(true).where(or.toArray(new Predicate[or.size()]));

		return em.createQuery(q).getResultList();
	}

	@Override
	public List<Song> findByKeywordsAndGenre(Set<String> keywords, long genreId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Song> q = cb.createQuery(Song.class);
		Root<Song> song = q.from(Song.class);
		List<Predicate> predicates = new ArrayList<>();
		for (String word : keywords) {
			predicates.add(cb.or(cb.like(song.get("title"), "%" + word + "%"),
					cb.like(song.get("singers"), "%" + word + "%")));

		}
		List<Predicate> or = new ArrayList<>();
		for (Predicate p : predicates) {
			or.add(cb.or(p));
		}

		Genre genre = genreRepository.getOne(genreId);
		List<Predicate> and = new ArrayList<>();
		and.add(cb.equal(song.get("genre"), genre));
		and.addAll(or);

		q.select(song).distinct(true).where(and.toArray(new Predicate[or.size()]));

		return em.createQuery(q).getResultList();
	}

}
