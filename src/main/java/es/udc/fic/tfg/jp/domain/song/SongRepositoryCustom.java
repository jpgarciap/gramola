package es.udc.fic.tfg.jp.domain.song;

import java.util.List;
import java.util.Set;

public interface SongRepositoryCustom {

	// @Query("select s from Song s where s.singers like %:keywords% or s.title like
	// %:keywords% order by s.title")
	List<Song> findByKeywords(Set<String> keywords);

	// @Query("select s from Song s where (s.singers like %:keywords% or s.title
	// like %:keywords%) and s.genre.genreId=:genreId order by s.title")
	List<Song> findByKeywordsAndGenre(Set<String> keywords, long genreId);

}
