package es.udc.fic.tfg.jp.web.editablePlaylist;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.udc.fic.tfg.jp.domain.genre.Genre;
import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.service.playlist.OrderSongPlDto;
import es.udc.fic.tfg.jp.service.playlist.PlaylistService;
import es.udc.fic.tfg.jp.service.song.SongService;
import es.udc.fic.tfg.jp.service.user.UserService;
import es.udc.fic.tfg.jp.web.support.Ajax;

@Controller
public class EditablePlaylistController {

	@Autowired
	PlaylistService playlistService;

	@Autowired
	SongService songService;

	@Autowired
	UserService userService;

	@GetMapping("/admin/editablePlaylist")
	public String getEditablePlaylist(Model model,
			@RequestHeader(value = "X-Requested-With", required = false) String requestedWith) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findByEmail(authentication.getName());
		String duration = playlistService.getDurationEditablePlaylistHoursMinSeconds(user.getUserId());
		model.addAttribute("duration", duration);
		List<Genre> genreList = new ArrayList<>();
		Genre all = new Genre("All");
		all.setGenreId((long) -1);
		genreList.add(all);
		genreList.addAll(songService.getAllGenreOrderByName());
		model.addAttribute("genreList", genreList);
		model.addAttribute("aleatoryFilters", new AleatoryFilters());
		model.addAttribute("dateForDatePicker", LocalDate.now().minusMonths(1).toDate());
		model.addAttribute(new CreatePlaylistForm());
		if (Ajax.isAjaxRequest(requestedWith)) {
			return "/admin/editablePlaylist".concat(" :: createPlaylistForm");
		}

		return "/admin/editablePlaylist";

	}

	@RequestMapping(value = "/admin/editablePlaylistData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<OrderSongPlDto> getSongsData() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findByEmail(authentication.getName());

		List<OrderSongPlDto> orderSongDtoList = playlistService
				.getOrderSongDtoEditablePlaylistFromUser(user.getUserId());

		return orderSongDtoList;

	}

	@PostMapping("/admin/editablePlaylist")
	String signup(@Valid @ModelAttribute CreatePlaylistForm createPlaylistForm, Errors errors, RedirectAttributes ra) {
		if (errors.hasErrors()) {
			return "/admin/editablePlaylist";
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findByEmail(authentication.getName());
		playlistService.createPlayList(createPlaylistForm.getName(), createPlaylistForm.getDescription(),
				user.getUserId());

		return "redirect:/admin/playlists";
	}

	@PostMapping("/admin/deleteSong")
	String deleteSong(@RequestBody long orderSongId) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findByEmail(authentication.getName());
		List<Long> orderSongIdList = new ArrayList<>();
		orderSongIdList.add(orderSongId);
		playlistService.removeSongsEditablePlaylist(orderSongIdList, user.getUserId());
		return "redirect:/admin/editablePlaylist";
	}

	@PostMapping("/admin/editablePlaylistAleatory")
	String aleatorySongs(@ModelAttribute AleatoryFilters aleatoryFilters) throws ParseException {
		if (aleatoryFilters.getGenreIdList().isEmpty()) {
			aleatoryFilters.getGenreIdList().add((long) -1);
		}

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findByEmail(authentication.getName());
		playlistService.generateAleatoryEditablePlayList(aleatoryFilters, user.getUserId());

		return "redirect:/admin/editablePlaylist";

	}

	@PostMapping("/admin/editablePlaylistChangeOrder")
	String changeOrder(@RequestBody OrderSongPlDto[] listData) {

		playlistService.changeOrderSongsEditablePlaylist(Arrays.asList(listData));
		return "redirect:/admin/editablePlaylist";

	}

}
