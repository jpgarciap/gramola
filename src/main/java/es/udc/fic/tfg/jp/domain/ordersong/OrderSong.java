package es.udc.fic.tfg.jp.domain.ordersong;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.udc.fic.tfg.jp.domain.song.Song;

@Entity
@Table(name = "orderSong")
public class OrderSong {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long orderSongId;
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "songId")
	private Song song;

	private int position;

	public OrderSong() {

	}

	public OrderSong(Song song, int position) {
		super();
		this.song = song;
		this.position = position;
	}

	public Song getSong() {
		return song;
	}

	public void setSong(Song song) {
		this.song = song;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int order) {
		this.position = order;
	}

	public long getOrderSongId() {
		return orderSongId;
	}

	public void setOrderSongId(long orderSongId) {
		this.orderSongId = orderSongId;
	}

	public int compareTo(OrderSong o) {
		return (this.position - o.position);
	}

}
