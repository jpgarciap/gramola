package es.udc.fic.tfg.jp.web.signup;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.springframework.stereotype.Component;

import es.udc.fic.tfg.jp.domain.user.UserRepository;

@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = EmailExistsValidator.class)
@Documented
public @interface EmailExists {

	String message()

	default "";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}

@Component
class EmailExistsValidator implements ConstraintValidator<EmailExists, String> {

	private final UserRepository userRepository;

	public EmailExistsValidator(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void initialize(EmailExists constraintAnnotation) {

	}

	public boolean isValid(String value, ConstraintValidatorContext context) {
		return !userRepository.exists(value);
	}

}
