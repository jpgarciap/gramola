package es.udc.fic.tfg.jp.domain.event;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.LocalTime;

import es.udc.fic.tfg.jp.domain.playlist.Playlist;

@Entity
@Table(name = "event")
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long eventId;
	@Column(unique = true)
	private String name;
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean monday;
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean tuesday;
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean wednesday;
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean thursday;
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean friday;
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean saturday;
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean sunday;
	@Basic
	@Temporal(TemporalType.TIME)
	private Date reproductionHour;
	private Date creationDate;
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "playlistId")
	private Playlist playlist;

	public Event(String name, boolean monday, boolean tuesday, boolean wednesday, boolean thursday, boolean friday,
			boolean saturday, boolean sunday, Date reproductionHour) {
		super();
		this.name = name;
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;

		this.creationDate = new Date();
		this.saturday = saturday;
		this.sunday = sunday;
		this.reproductionHour = reproductionHour;
	}

	public Event() {

	}

	public Event(int day, Playlist playlist, Date reproductionHour, String eventName) {
		switch (day) {
		case 1:
			this.monday = true;
			break;
		case 2:
			this.tuesday = true;
			break;
		case 3:
			this.wednesday = true;
			break;
		case 4:
			this.thursday = true;
			break;
		case 5:
			this.friday = true;
			break;
		case 6:
			this.saturday = true;
			break;
		case 7:
			this.sunday = true;
			break;
		default:
			this.monday = true;
		}

		this.creationDate = new Date();

		this.name = eventName;
		this.playlist = playlist;
		this.reproductionHour = reproductionHour;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public boolean isMonday() {
		return monday;
	}

	public void setMonday(boolean monday) {
		this.monday = monday;
	}

	public boolean isTuesday() {
		return tuesday;
	}

	public Date getReproductionHour() {
		return reproductionHour;
	}

	public void setReproductionHour(Date reproductionHour) {
		this.reproductionHour = reproductionHour;
	}

	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}

	public boolean isWednesday() {
		return wednesday;
	}

	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}

	public boolean isThursday() {
		return thursday;
	}

	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}

	public boolean isFriday() {
		return friday;
	}

	public void setFriday(boolean friday) {
		this.friday = friday;
	}

	public boolean isSaturday() {
		return saturday;
	}

	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}

	public boolean isSunday() {
		return sunday;
	}

	public void setSunday(boolean sunday) {
		this.sunday = sunday;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public LocalTime getLocalTime() {
		SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
		String strTime = localDateFormat.format(getReproductionHour());
		LocalTime t = LocalTime.parse(strTime);
		return t;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Playlist getPlaylist() {
		return playlist;
	}

	public void setPlaylist(Playlist playlist) {
		this.playlist = playlist;
	}

}
