package es.udc.fic.tfg.jp.domain.playlist;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaylistRepository extends JpaRepository<Playlist, Long> {

	@Query("select count(p) > 0 from Playlist p where p.name = :name")
	boolean existsByName(@Param("name") String name);

	@Query("select p from Playlist p order by p.name")
	List<Playlist> getAllPlaylistsOrderByName();

	Playlist findByName(String name);

}
