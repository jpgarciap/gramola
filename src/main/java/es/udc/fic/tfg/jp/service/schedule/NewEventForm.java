package es.udc.fic.tfg.jp.service.schedule;

public class NewEventForm {

	private String hour;
	private long playlistId;
	private int day;

	public NewEventForm() {
	}

	public NewEventForm(int day, String hour, long playlistId) {
		super();
		this.day = day;
		this.hour = hour;
		this.playlistId = playlistId;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String reproductionDate) {
		this.hour = reproductionDate;
	}

	public long getPlaylistId() {
		return playlistId;
	}

	public void setPlaylistId(long playlistId) {
		this.playlistId = playlistId;
	}

}
