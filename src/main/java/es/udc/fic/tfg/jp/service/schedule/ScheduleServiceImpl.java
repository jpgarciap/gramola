package es.udc.fic.tfg.jp.service.schedule;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import es.udc.fic.tfg.jp.domain.event.Event;
import es.udc.fic.tfg.jp.domain.event.EventRepository;
import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;
import es.udc.fic.tfg.jp.domain.ordersong.OrderSongRepository;
import es.udc.fic.tfg.jp.domain.playlist.Playlist;
import es.udc.fic.tfg.jp.domain.playlist.PlaylistRepository;
import es.udc.fic.tfg.jp.service.playlist.PlaylistService;
import es.udc.fic.tfg.jp.web.schedule.EventDto;

@Service
public class ScheduleServiceImpl implements ScheduleService {

	@Autowired
	private PlaylistRepository playlistRepository;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private PlaylistService playlistService;

	@Autowired
	private ScheduleService scheduleService;

	@Autowired
	private OrderSongRepository orderSongRepository;

	@Value("${playlist.path}")
	private String playlistPath;

	@Value("${song.path}")
	private String songPath;

	@Override
	public Event newEvent(NewEventForm newEventForm) {
		String playlistAndEventName = getEventName(newEventForm.getDay(), newEventForm.getHour());
		Event event = eventRepository.findByName(playlistAndEventName);
		Playlist pl = playlistRepository.findByName(playlistAndEventName);
		if (pl != null)
			playlistService.removePlayList(pl.getPlaylistId());

		if (event != null) {
			scheduleService.deleteEvent(event.getEventId());
		}
		Playlist playlist = playlistRepository.getOne(newEventForm.getPlaylistId());
		Playlist plNew = new Playlist(playlistAndEventName, playlist.getDescription(), playlist.getPath(),
				playlist.getDuration());

		List<OrderSong> orderSongList = new ArrayList<>();
		for (OrderSong oS : playlist.getOrderSongs()) {
			OrderSong oSNew = orderSongRepository.save(new OrderSong(oS.getSong(), oS.getPosition()));
			orderSongList.add(oSNew);
		}
		plNew.setOrderSongs(orderSongList);
		plNew.setPath(playlistPath + plNew.getName() + ".lst");
		plNew = playlistRepository.save(plNew);
		createPlaylistZarafile(plNew);
		DateFormat formatter = new SimpleDateFormat("HH:mm");
		Date date = null;
		try {
			date = formatter.parse(newEventForm.getHour());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		event = new Event(newEventForm.getDay(), plNew, date, playlistAndEventName);

		return eventRepository.save(event);

	}

	private String getEventName(int day, String reproductionHour) {
		String name;
		String[] split = reproductionHour.split(":");
		switch (day) {
		case 1:
			name = "mon";
			break;
		case 2:
			name = "tue";
			break;
		case 3:
			name = "wed";
			break;
		case 4:
			name = "thu";
			break;
		case 5:
			name = "fri";
			break;
		case 6:
			name = "sat";
			break;
		case 7:
			name = "sun";
			break;
		default:
			name = "mon";
		}

		return name + split[0];
	}

	private void createPlaylistZarafile(Playlist playlist) {

		FileOutputStream fos = null;
		File file;

		try {
			playlist.setPath(playlist.getPath());
			file = new File(playlist.getPath());
			fos = new FileOutputStream(file);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			bw.write(String.valueOf(playlist.getOrderSongs().size()));
			for (OrderSong orderSong : playlist.getOrderSongs()) {
				long duration = orderSong.getSong().getDuration();
				String path = songPath + orderSong.getSong().getPath();
				String line = String.valueOf(duration) + "\t" + path;
				bw.newLine();
				bw.write(line);
			}
			bw.flush();
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<EventDto> getAllEvents() {
		List<EventDto> resultList = new ArrayList<>();
		List<Event> eventList = eventRepository.findAll();

		for (Event e : eventList) {
			resultList.add(new EventDto(e));
		}
		return resultList;
	}

	@Override
	public void deleteEvent(long eventId) {
		eventRepository.deleteById(eventId);

	}

}
