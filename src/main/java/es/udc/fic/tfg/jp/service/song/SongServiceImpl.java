package es.udc.fic.tfg.jp.service.song;

import static java.lang.Math.toIntExact;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import es.udc.fic.tfg.jp.domain.dislikedsong.DislikedSong;
import es.udc.fic.tfg.jp.domain.dislikedsong.DislikedSongRepository;
import es.udc.fic.tfg.jp.domain.event.Event;
import es.udc.fic.tfg.jp.domain.event.EventRepository;
import es.udc.fic.tfg.jp.domain.genre.Genre;
import es.udc.fic.tfg.jp.domain.genre.GenreRepository;
import es.udc.fic.tfg.jp.domain.likedsong.LikedSong;
import es.udc.fic.tfg.jp.domain.likedsong.LikedSongRepository;
import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;
import es.udc.fic.tfg.jp.domain.playlist.Playlist;
import es.udc.fic.tfg.jp.domain.playlist.PlaylistRepository;
import es.udc.fic.tfg.jp.domain.song.Song;
import es.udc.fic.tfg.jp.domain.song.SongRepository;
import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.domain.user.UserRepository;
import es.udc.fic.tfg.jp.service.song.comparators.SongDtoChainedComparator;
import es.udc.fic.tfg.jp.service.song.comparators.SongDtoDateComparator;
import es.udc.fic.tfg.jp.service.song.comparators.SongDtoLikesComparator;
import es.udc.fic.tfg.jp.service.song.comparators.SongDtoSingersComparator;
import es.udc.fic.tfg.jp.service.song.comparators.SongDtoTitleComparator;
import es.udc.fic.tfg.jp.web.song.FindSongForm;
import es.udc.fic.tfg.jp.web.support.Pageable;

@Service
public class SongServiceImpl implements SongService {

	@Autowired
	private SongRepository songRepository;

	@Autowired
	private GenreRepository genreRepository;

	@Autowired
	private LikedSongRepository likedSongRepository;

	@Autowired
	private DislikedSongRepository dislikedSongRepository;

	@Autowired
	private PlaylistRepository playlistRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EventRepository eventRepository;

	@Value("${latencia.audio}")
	private int latencia;

	private long countLikesSongSinceDate(long songId, LocalDate date) {
		Song song = songRepository.getOne(songId);
		return likedSongRepository.countBySongAndDate(song, date.toDate());
	}

	private long countDislikesSongSinceDate(long songId, LocalDate date) {
		Song song = songRepository.getOne(songId);
		return dislikedSongRepository.countBySongAndDate(song, date.toDate());
	}

	@Override
	public List<Genre> getAllGenreOrderByName() {

		return genreRepository.findAllOrderByName();
	}

	private List<Event> getEventListFilterByDay(Date date) {
		String day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()).toLowerCase();
		List<Event> eventsFiltersByDay = new ArrayList<>();
		List<Event> allEvents = eventRepository.findAll();
		switch (day) {
		case "monday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isMonday()).collect(Collectors.toList());
			break;
		case "tuesday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isTuesday()).collect(Collectors.toList());
			;
			break;
		case "wednesday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isWednesday()).collect(Collectors.toList());
			;
			break;
		case "thursday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isThursday()).collect(Collectors.toList());
			;
			break;
		case "friday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isFriday()).collect(Collectors.toList());
			;
			break;
		case "saturday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isSaturday()).collect(Collectors.toList());
			;
			break;
		case "sunday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isSunday()).collect(Collectors.toList());
			;
			break;
		}

		return eventsFiltersByDay;
	}

	private List<OrderSong> getOrderSongsOrderByPosition(long playlistId) {
		List<OrderSong> orderSongList = new ArrayList<>();
		Playlist playlist = playlistRepository.getOne(playlistId);
		orderSongList = playlist.getOrderSongs();
		orderSongList.sort(Comparator.comparing(OrderSong::getPosition));
		return orderSongList;
	}

	private Song getSongLikeOrDislike(Date date) {
		// Se añaden los eventos de ayer y de hoy porque un evento puede empezar a las
		// 23:55 por ejemplo y le habriamos dado like al dia siguiente
		List<Event> eventsFiltersByDay = getEventListFilterByDay(new LocalDate(date).minusDays(1).toDate());
		eventsFiltersByDay.addAll(getEventListFilterByDay(date));
		if (eventsFiltersByDay.size() == 0)
			return null;
		List<Event> eventsFiltersByTime = new ArrayList<>();

		SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
		String strTime = localDateFormat.format(date);
		LocalTime time = LocalTime.parse(strTime);

		// filterByTime
		eventsFiltersByTime = eventsFiltersByDay.stream().filter(e -> e.getLocalTime().isBefore(time))
				.collect(Collectors.toList());
		if (eventsFiltersByTime.size() == 0) {
			return null;
		}

		// Obtenemos el evento más proximo al momento del like
		Collections.sort(eventsFiltersByTime, (o1, o2) -> o1.getLocalTime().compareTo(o2.getLocalTime()));
		Event event = eventsFiltersByTime.get(eventsFiltersByTime.size() - 1);
		String strEventTime = localDateFormat.format(event.getReproductionHour());
		// Intentamos obtener la canción
		List<OrderSong> orderSongs = getOrderSongsOrderByPosition(event.getPlaylist().getPlaylistId());
		Song song;
		LocalTime countTime = LocalTime.parse(strEventTime);

		if ((LocalTime.parse(strEventTime).plusMillis(toIntExact(event.getPlaylist().getDuration()))).isBefore(time))
			return null;

		for (OrderSong orderSong : orderSongs) {
			song = orderSong.getSong();
			countTime = countTime.plusMillis(toIntExact(song.getDuration()));

			if (countTime.isAfter(time)) {
				return song;
			}
		}

		return null;
	}

	// Si el usuario ya le ha dado like, al volverle a dar simplemente actualizamos
	// la fecha
	@Override
	public String userLikeSong(User user, Date date) {
		// restamos la latencia
		LocalDateTime lc = new LocalDateTime(date);
		lc.minusMillis(latencia);
		Song song = getSongLikeOrDislike(lc.toDate());
		if (song == null)
			return "No Streaming";
		else {
			LikedSong likedSong = likedSongRepository.findByUserAndSong(user, song);
			DislikedSong dislikedSong = dislikedSongRepository.findByUserAndSong(user, song);
			// borramos el dislike si lo hay
			if (dislikedSong != null) {
				dislikedSongRepository.delete(dislikedSong);
			}

			if (likedSong == null) {
				likedSong = new LikedSong(song, user, new Date());
				likedSongRepository.save(likedSong);
				user.getLikes().add(likedSong);
			} else {
				likedSong.setDate(new Date());
			}
			userRepository.save(user);
			return song.getTitle();
		}

	}

	@Override
	public String userDislikeSong(User user, Date date) {
		// restamos la latencia
		LocalDateTime lc = new LocalDateTime(date);
		lc.minusMillis(latencia);
		Song song = getSongLikeOrDislike(lc.toDate());
		if (song == null)
			return "No Streaming";
		else {
			LikedSong likedSong = likedSongRepository.findByUserAndSong(user, song);
			DislikedSong dislikedSong = dislikedSongRepository.findByUserAndSong(user, song);
			if (likedSong != null) {
				likedSongRepository.delete(likedSong);
			}

			if (dislikedSong == null) {
				dislikedSong = new DislikedSong(song, user, new Date());
				dislikedSongRepository.save(dislikedSong);
				user.getDislikes().add(dislikedSong);
			} else {
				dislikedSong.setDate(new Date());
			}
			userRepository.save(user);
			return song.getTitle();
		}
	}

	private String milisecondsToMinutesAndSeconds(long miliseconds) {
		long seconds = Math.round(Float.valueOf(miliseconds) / 1000);
		int mins = (int) (seconds / 60);
		int resto = (int) (seconds - mins * 60);

		return String.format("%02d", mins) + ":" + String.format("%02d", resto);

	}

	@Override
	public Pageable<SongDto> findSongs(FindSongForm filters, PageRequest pageRequest) {

		List<Song> songList = new ArrayList<>();
		List<SongDto> songDtoList = new ArrayList<>();
		List<SongDto> songDtoListResult = new ArrayList<>();
		List<Comparator<SongDto>> listComparators = new ArrayList<>();
		String keywords = filters.getKeywords().toLowerCase();
		LocalDate date = filters.getDate();

		if (filters.getGenreId() == -1)
			songList = songRepository.findByKeywords(new HashSet<String>(Arrays.asList(keywords.split(" "))));
		else {
			songList = songRepository.findByKeywordsAndGenre(new HashSet<String>(Arrays.asList(keywords.split(" "))),
					filters.getGenreId());
		}

		songDtoList = songToSongDtoList(songList, date);
		// Ordenación
		switch (filters.getOrderBy()) {
		case "Title":
			listComparators.add(new SongDtoTitleComparator());
			break;
		case "Singers":
			listComparators.add(new SongDtoSingersComparator());
			break;
		case "Likes":
			listComparators.add(new SongDtoLikesComparator());
			break;
		case "Last Reproduction":
			listComparators.add(new SongDtoDateComparator());
			break;
		default:
			listComparators.add(new SongDtoTitleComparator());
		}

		Collections.sort(songDtoList, new SongDtoChainedComparator(listComparators));

		// ascendente o descendente
		if (filters.isAsc())
			songDtoListResult = Lists.reverse(songDtoList);
		else
			songDtoListResult = songDtoList;

		Pageable<SongDto> pages = new Pageable<SongDto>(songDtoListResult);
		pages.setPageSize(pageRequest.getPageSize());
		pages.setPage(pageRequest.getPageNumber());
		return pages;

	}

	private List<SongDto> songToSongDtoList(List<Song> songList, LocalDate localDate) {
		List<SongDto> resultList = new ArrayList<>();

		for (Song song : songList) {
			long duration = Math.round(Float.valueOf(song.getDuration()) / 1000);
			long likes = countLikesSongSinceDate(song.getSongId(), localDate)
					- countDislikesSongSinceDate(song.getSongId(), localDate);
			String durationMinSec = milisecondsToMinutesAndSeconds(song.getDuration());
			resultList.add(new SongDto(song, likes, duration, durationMinSec));
		}

		return resultList;
	}

}
