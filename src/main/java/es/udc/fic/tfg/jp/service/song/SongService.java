package es.udc.fic.tfg.jp.service.song;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;

import es.udc.fic.tfg.jp.domain.genre.Genre;
import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.web.song.FindSongForm;
import es.udc.fic.tfg.jp.web.support.Pageable;

public interface SongService {

	String userLikeSong(User user, Date date);

	String userDislikeSong(User user, Date date);

	List<Genre> getAllGenreOrderByName();

	Pageable<SongDto> findSongs(FindSongForm filters, PageRequest pageRequest);

}
