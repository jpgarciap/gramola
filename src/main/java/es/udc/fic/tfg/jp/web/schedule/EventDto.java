package es.udc.fic.tfg.jp.web.schedule;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import es.udc.fic.tfg.jp.domain.event.Event;

public class EventDto {

	private long id;
	private String title;
	private String start;
	private String end;
	private List<Long> dow;

	public EventDto(Event event) {
		this.id = event.getEventId();
		DateTime dateInit = new DateTime(event.getReproductionHour());
		DateTime dateEnd = dateInit.plusHours(1);
		this.title = event.getName();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm:ss");
		String strStart = dateInit.toString(fmt);
		String strEnd = dateEnd.toString(fmt);
		this.start = strStart;
		this.end = strEnd;

		this.dow = new ArrayList<>();

		if (event.isMonday()) {
			this.dow.add((long) 1);
		}
		if (event.isTuesday()) {
			this.dow.add((long) 2);
		}
		if (event.isWednesday()) {
			this.dow.add((long) 3);
		}
		if (event.isThursday()) {
			this.dow.add((long) 4);
		}
		if (event.isFriday()) {
			this.dow.add((long) 5);
		}
		if (event.isSaturday()) {
			this.dow.add((long) 6);
		}
		if (event.isSunday()) {
			this.dow.add((long) 0);
		}

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public List<Long> getDow() {
		return dow;
	}

	public void setDow(List<Long> dow) {
		this.dow = dow;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
