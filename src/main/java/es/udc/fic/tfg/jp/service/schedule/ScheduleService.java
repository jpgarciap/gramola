package es.udc.fic.tfg.jp.service.schedule;

import java.util.List;

import es.udc.fic.tfg.jp.domain.event.Event;
import es.udc.fic.tfg.jp.web.schedule.EventDto;

public interface ScheduleService {

	Event newEvent(NewEventForm newEventForm);

	List<EventDto> getAllEvents();

	void deleteEvent(long eventId);

}
