package es.udc.fic.tfg.jp.domain.dislikedsong;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.udc.fic.tfg.jp.domain.song.Song;
import es.udc.fic.tfg.jp.domain.user.User;

@Repository
public interface DislikedSongRepository extends JpaRepository<DislikedSong, Long> {

	DislikedSong findByUserAndSong(User user, Song song);

	@Query("select count(d) from DislikedSong d where d.song = :song and d.date > :date")
	long countBySongAndDate(@Param("song") Song song, @Param("date") Date date);

	long countBySong(Song song);
}
