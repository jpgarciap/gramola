package es.udc.fic.tfg.jp.web.song;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.service.playlist.PlaylistService;
import es.udc.fic.tfg.jp.service.user.UserService;

@RestController
public class SongControllerRest {

	@Autowired
	PlaylistService playlistService;

	@Autowired
	UserService userService;

	@PostMapping("/admin/songs")
	public String saveSongsEditablePlaylist(@RequestBody List<Long> songIdList) {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findByEmail(authentication.getName());
		if (songIdList.size() > 0)
			playlistService.addSongsEditablePlaylist(songIdList, user.getUserId());

		return "Success";
	}

}
