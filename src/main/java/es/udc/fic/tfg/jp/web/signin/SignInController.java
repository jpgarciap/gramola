package es.udc.fic.tfg.jp.web.signin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SignInController {

	@GetMapping("/signin")
	public String signin() {
		return "/signin";
	}

}
