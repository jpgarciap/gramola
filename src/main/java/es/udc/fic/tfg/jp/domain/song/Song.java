package es.udc.fic.tfg.jp.domain.song;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.udc.fic.tfg.jp.domain.genre.Genre;

@Entity
@Table(name = "song")
public class Song {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long songId;
	// name es el title-singers
	private String name;
	private String title;
	private String path;
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "genreId")
	private Genre genre;
	private String format;
	private long duration;
	private String singers;
	private Date lastReproduction;

	private long count;

	public Song() {
	}

	public Song(String name, Genre genre, String format, long duration, String title, String singers) {
		this.name = name;
		this.title = title;
		this.genre = genre;
		this.format = format;
		this.duration = duration;
		this.singers = singers;
	}

	public Song(String name, Genre genre, String format, long duration, String title, String path, String singers) {
		this.name = name;
		this.title = title;
		this.path = path;
		this.genre = genre;
		this.format = format;
		this.duration = duration;
		this.singers = singers;
	}

	public Song(String name, Genre genre, String format, long duration, String title, String path, String singers,
			long count) {
		this.name = name;
		this.title = title;
		this.path = path;
		this.genre = genre;
		this.format = format;
		this.duration = duration;
		this.singers = singers;
		this.count = count;
	}

	public long getSongId() {
		return songId;
	}

	public void setSongId(long songId) {
		this.songId = songId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getSingers() {
		return singers;
	}

	public void setSingers(String singers) {
		this.singers = singers;
	}

	public Date getLastReproduction() {
		return lastReproduction;
	}

	public void setLastReproduction(Date lastPlay) {
		this.lastReproduction = lastPlay;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

}
