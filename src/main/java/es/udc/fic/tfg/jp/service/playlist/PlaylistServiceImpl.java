package es.udc.fic.tfg.jp.service.playlist;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import es.udc.fic.tfg.jp.domain.dislikedsong.DislikedSongRepository;
import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylist;
import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylistRepository;
import es.udc.fic.tfg.jp.domain.event.EventRepository;
import es.udc.fic.tfg.jp.domain.likedsong.LikedSongRepository;
import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;
import es.udc.fic.tfg.jp.domain.ordersong.OrderSongRepository;
import es.udc.fic.tfg.jp.domain.playlist.Playlist;
import es.udc.fic.tfg.jp.domain.playlist.PlaylistRepository;
import es.udc.fic.tfg.jp.domain.song.Song;
import es.udc.fic.tfg.jp.domain.song.SongRepository;
import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.domain.user.UserRepository;
import es.udc.fic.tfg.jp.service.song.SongDto;
import es.udc.fic.tfg.jp.service.song.comparators.SongDtoChainedComparator;
import es.udc.fic.tfg.jp.service.song.comparators.SongDtoLikesComparator;
import es.udc.fic.tfg.jp.web.editablePlaylist.AleatoryFilters;

@Service
public class PlaylistServiceImpl implements PlaylistService {

	@Autowired
	EditablePlaylistRepository editablePlaylistRepository;

	@Autowired
	DislikedSongRepository dislikedSongRepository;

	@Autowired
	LikedSongRepository likedSongRepository;

	@Autowired
	PlaylistRepository playlistRepository;

	@Autowired
	SongRepository songRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	EventRepository eventRepository;

	@Autowired
	OrderSongRepository orderSongRepository;

	@Value("${playlist.path}")
	private String playlistPath;

	@Value("${song.path}")
	private String songPath;

	@Override
	public void addSongsEditablePlaylist(List<Long> songIdList, long userId) {
		long duration = 0;
		User user = userRepository.getOne(userId);
		EditablePlaylist editablePlaylist = user.getEditablePlaylist();
		for (long songId : songIdList) {
			Song song = songRepository.getOne(songId);
			editablePlaylist.getOrderSongs().add(new OrderSong(song, editablePlaylist.getOrderSongs().size() + 1));
			duration += song.getDuration();
		}
		user.getEditablePlaylist().addDuration(duration);
		editablePlaylistRepository.save(user.getEditablePlaylist());
	}

	@Override
	public void removeSongsEditablePlaylist(List<Long> orderSongIdList, long userId) {
		long duration = 0;
		int count = 1;
		User user = userRepository.getOne(userId);
		// borramos las canciones
		for (long orderSongId : orderSongIdList) {
			OrderSong orderSong = orderSongRepository.getOne(orderSongId);
			user.getEditablePlaylist().getOrderSongs().remove(orderSong);
			duration += orderSong.getSong().getDuration();

		}
		// actualizamos las posiciones
		for (OrderSong orderSong : user.getEditablePlaylist().getOrderSongs()) {
			orderSong.setPosition(count);
			count++;
		}
		user.getEditablePlaylist().subtractDuration(duration);
		editablePlaylistRepository.save(user.getEditablePlaylist());
	}

	@Override
	public void changeOrderSongsEditablePlaylist(List<OrderSongPlDto> orderSongPlDtoList) {

		int min = orderSongPlDtoList.get(0).getPosition();
		for (OrderSongPlDto oSPlDto : orderSongPlDtoList) {
			if (oSPlDto.getPosition() < min)
				min = oSPlDto.getPosition();
		}

		for (OrderSongPlDto oSPlDto : orderSongPlDtoList) {
			OrderSong orderSong = orderSongRepository.getOne(oSPlDto.getOrderSongId());
			orderSong.setPosition(min);
			min++;
			orderSongRepository.save(orderSong);
		}
	}

	/////////////// Private methods aleatory songs///////////////////////
	private Song getRandomElementFromList(List<Song> songList) {
		return songList.get((int) Math.floor(Math.random() * songList.size()));

	}

	private List<Long> getNRandomElementsFromList(List<Song> songList, double numElements) {
		List<Long> resultSongIdList = new ArrayList<>();
		List<Song> localSongList = songList;
		if (songList.size() <= numElements) {
			for (Song song : songList)
				resultSongIdList.add(song.getSongId());

		} else {

			while (resultSongIdList.size() < numElements + 1) {
				Song song = getRandomElementFromList(localSongList);
				resultSongIdList.add(song.getSongId());
				localSongList.remove(song);
			}
		}
		return resultSongIdList;
	}

	private SongDto getSongDto(Song song, Date date) {
		long duration = Math.round(Float.valueOf(song.getDuration()) / 1000);
		long totalLikes = likedSongRepository.countBySongAndDate(song, date)
				- dislikedSongRepository.countBySongAndDate(song, date);
		String durationMinSec = milisecondsToMinutesAndSeconds(duration);
		SongDto songDto = new SongDto(song, totalLikes, duration, durationMinSec);
		return songDto;

	}

	private List<Long> getNSongsFromListOrderByLikesOrLastReproduction(List<Song> songList, double numElements,
			Date date, boolean orderByLikes) {
		List<SongDto> songDtoList = new ArrayList<>();
		List<Long> resultList = new ArrayList<>();
		List<Comparator<SongDto>> listComparators = new ArrayList<>();

		for (Song song : songList) {
			songDtoList.add(getSongDto(song, date));
		}

		if (orderByLikes) {
			listComparators.add(new SongDtoLikesComparator());
		} else {
			List<Song> aux = songList;
			songList.clear();
			for (Song song : aux) {
				if (song.getLastReproduction() == null) {
					songList.add(song);
				} else if (song.getLastReproduction().after(date)) {
					songList.add(song);
				}
			}
		}

		// ordenamos
		Collections.sort(songDtoList, new SongDtoChainedComparator(listComparators));
		for (SongDto songDto : songDtoList)
			resultList.add(songDto.getSong().getSongId());

		// cogemos los N elementos
		if (resultList.size() > numElements)
			resultList = resultList.stream().limit((long) numElements).collect(Collectors.toList());

		return resultList;
	}

	////////////////////////////////////////////////////

	@Override
	public void generateAleatoryEditablePlayList(AleatoryFilters aleatoryFilters, long userId) throws ParseException {
		int numGenre;
		// nos dice el numero de canciones en cada genero
		double numSongsInGenre;
		boolean allGenres = false;
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		List<Long> resultSongIdList = new ArrayList<>();
		List<Song> songList = new ArrayList<>();

		if (aleatoryFilters.getGenreIdList().contains((long) -1)) {
			numGenre = 1;
			allGenres = true;
		} else {
			numGenre = aleatoryFilters.getGenreIdList().size();
		}
		numSongsInGenre = Math.ceil((aleatoryFilters.getMaxSongs() / numGenre));

		for (long genreId : aleatoryFilters.getGenreIdList()) {
			if (allGenres) {
				songList = songRepository.findAll();
				if (aleatoryFilters.getTop().equals("likes"))
					resultSongIdList = getNSongsFromListOrderByLikesOrLastReproduction(songList, numSongsInGenre,
							formatter.parse(aleatoryFilters.getLocalDate()), true);
				else if (aleatoryFilters.getTop().equals("lastReproduction"))
					resultSongIdList = getNSongsFromListOrderByLikesOrLastReproduction(songList, numSongsInGenre,
							formatter.parse(aleatoryFilters.getLocalDate()), false);
				else
					resultSongIdList = getNRandomElementsFromList(songList, numSongsInGenre);
				break;
			} else {
				songList = songRepository.findByGenre(genreId);
				if (aleatoryFilters.getTop().equals("likes"))
					resultSongIdList.addAll(getNSongsFromListOrderByLikesOrLastReproduction(songList, numSongsInGenre,
							formatter.parse(aleatoryFilters.getLocalDate()), true));

				else if (aleatoryFilters.getTop().equals("lastReproduction"))
					resultSongIdList = getNSongsFromListOrderByLikesOrLastReproduction(songList, numSongsInGenre,
							formatter.parse(aleatoryFilters.getLocalDate()), false);
				else
					resultSongIdList.addAll(getNRandomElementsFromList(songList, numSongsInGenre));
			}
		}
		if (resultSongIdList.size() > aleatoryFilters.getMaxSongs())
			resultSongIdList = resultSongIdList.stream().limit((long) aleatoryFilters.getMaxSongs())
					.collect(Collectors.toList());

		addSongsEditablePlaylist(resultSongIdList, userId);

	}

	// nos ordena la lista por posicion
	private List<OrderSong> getOrderSongListOrderByPosition(long userId) {
		User user = userRepository.getOne(userId);
		List<OrderSong> resultList = new ArrayList<>();
		resultList = user.getEditablePlaylist().getOrderSongs();
		resultList.sort(Comparator.comparing(OrderSong::getPosition));
		return resultList;
	}

	private List<OrderSongPlDto> orderSongToOrderSongDto(List<OrderSong> orderSongList) {
		List<OrderSongPlDto> resultList = new ArrayList<>();
		for (OrderSong orderSong : orderSongList) {
			long likes = likedSongRepository.countBySong(orderSong.getSong())
					- dislikedSongRepository.countBySong(orderSong.getSong());
			resultList.add(new OrderSongPlDto(orderSong, likes,
					milisecondsToMinutesAndSeconds(orderSong.getSong().getDuration())));
		}
		return resultList;

	}

	@Override
	public List<OrderSongPlDto> getOrderSongDtoEditablePlaylistFromUser(long userId) {
		List<OrderSong> orderSongList = getOrderSongListOrderByPosition(userId);
		List<OrderSongPlDto> resultList = new ArrayList<>();
		resultList = orderSongToOrderSongDto(orderSongList);
		return resultList;
	}

	private void createPlaylistZarafile(Playlist playlist) {

		FileOutputStream fos = null;
		File file;

		try {
			playlist.setPath(playlist.getPath());
			file = new File(playlist.getPath());
			fos = new FileOutputStream(file);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			bw.write(String.valueOf(playlist.getOrderSongs().size()));
			for (OrderSong orderSong : playlist.getOrderSongs()) {
				long duration = orderSong.getSong().getDuration();
				String path = songPath + orderSong.getSong().getPath();
				String line = String.valueOf(duration) + "\t" + path;
				bw.newLine();
				bw.write(line);
			}
			bw.flush();
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Playlist createPlayList(String name, String description, long userId) {
		User user = userRepository.getOne(userId);
		EditablePlaylist editablePlaylist = user.getEditablePlaylist();
		Playlist playlist = new Playlist(name, description, editablePlaylist.getDuration(), new Date());
		playlist.getOrderSongs().addAll(getOrderSongListOrderByPosition(userId));
		playlistRepository.save(playlist);
		// vaciamos editableplaylist
		editablePlaylist.setDuration(0);
		editablePlaylist.getOrderSongs().clear();
		editablePlaylistRepository.save(editablePlaylist);
		// vaciamos

		// creamos el fichero para zara radio
		playlist.setPath(playlistPath + playlist.getName() + ".lst");
		createPlaylistZarafile(playlist);
		return playlistRepository.save(playlist);
	}

	private String milisecondsToHoursMinutesAndSeconds(long miliseconds) {

		long seconds = Math.round(Float.valueOf(miliseconds) / 1000);
		int hours = (int) seconds / 3600;
		int remainder = (int) seconds - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;

		return String.format("%02d", hours) + ":" + String.format("%02d", mins) + ":"
				+ String.format("%02d", remainder);
	}

	@Override
	public String getDurationEditablePlaylistHoursMinSeconds(long userId) {
		User user = userRepository.getOne(userId);
		long miliseconds = user.getEditablePlaylist().getDuration();
		return milisecondsToHoursMinutesAndSeconds(miliseconds);
	}

	@Override
	public List<PlaylistDto> getAllPlaylistDto() {
		List<PlaylistDto> resultList = new ArrayList<>();
		List<Playlist> playlists = playlistRepository.findAll();
		for (Playlist playlist : playlists) {
			List<OrderSongPlDto> orderSongPlDtoList = orderSongToOrderSongDto(playlist.getOrderSongs());
			resultList.add(new PlaylistDto(playlist, milisecondsToHoursMinutesAndSeconds(playlist.getDuration()),
					orderSongPlDtoList));
		}
		return resultList;
	}

	private void deletePlaylistZaraFile(Playlist playlist) {
		try {

			File file = new File(playlist.getPath());
			file.delete();

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@Override
	public boolean removePlayList(long playlistId) {
		Playlist playlist = playlistRepository.getOne(playlistId);
		deletePlaylistZaraFile(playlist);

		// borramos los eventos asociados
		eventRepository.deleteEventsByPlaylistId(playlist);

		playlistRepository.deleteById(playlistId);
		return !playlistRepository.existsById(playlistId);
	}

	private String milisecondsToMinutesAndSeconds(long miliseconds) {
		long seconds = Math.round(Float.valueOf(miliseconds) / 1000);
		int mins = (int) (seconds / 60);
		int resto = (int) (seconds - mins * 60);

		return String.format("%02d", mins) + ":" + String.format("%02d", resto);

	}

	@Override
	public Playlist updatePlayList(Playlist playlist) {
		return playlistRepository.save(playlist);

	}

	@Override
	public List<Playlist> getAllPlaylistsOrderByName() {
		return playlistRepository.getAllPlaylistsOrderByName();
	}

}
