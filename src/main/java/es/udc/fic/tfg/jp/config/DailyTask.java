package es.udc.fic.tfg.jp.config;

import static java.lang.Math.toIntExact;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import es.udc.fic.tfg.jp.domain.event.Event;
import es.udc.fic.tfg.jp.domain.event.EventRepository;
import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;
import es.udc.fic.tfg.jp.domain.playlist.Playlist;
import es.udc.fic.tfg.jp.domain.playlist.PlaylistRepository;
import es.udc.fic.tfg.jp.domain.song.Song;
import es.udc.fic.tfg.jp.domain.song.SongRepository;

@Component
public class DailyTask {

	@Autowired
	private PlaylistRepository playlistRepository;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private SongRepository songRepository;

	private List<Event> getEventListFilterByDay(Date date) {
		String day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()).toLowerCase();
		List<Event> eventsFiltersByDay = new ArrayList<>();
		List<Event> allEvents = eventRepository.findAll();
		switch (day) {
		case "monday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isMonday()).collect(Collectors.toList());
			break;
		case "tuesday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isTuesday()).collect(Collectors.toList());
			;
			break;
		case "wednesday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isWednesday()).collect(Collectors.toList());
			;
			break;
		case "thursday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isThursday()).collect(Collectors.toList());
			;
			break;
		case "friday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isFriday()).collect(Collectors.toList());
			;
			break;
		case "saturday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isSaturday()).collect(Collectors.toList());
			;
			break;
		case "sunday":
			eventsFiltersByDay = allEvents.stream().filter(e -> e.isSunday()).collect(Collectors.toList());
			;
			break;
		}

		return eventsFiltersByDay;
	}

	private List<OrderSong> getOrderSongsOrderByPosition(long playlistId) {
		List<OrderSong> orderSongList = new ArrayList<>();
		Playlist playlist = playlistRepository.getOne(playlistId);
		orderSongList = playlist.getOrderSongs();
		Collections.sort(orderSongList, (o1, o2) -> o1.compareTo(o2));
		return orderSongList;
	}

	// Indica cada cuanto se actualizan las canciones y playlist emitidas, en este
	// caso todos los días a las 23:55
	// https://www.freeformatter.com/cron-expression-generator-quartz.html
	// cada 20 segundos: "0/20 * * * * ?"
	@Scheduled(cron = "0 55 23 * * ?")
	public void updateReproducedSongsAndPlaylists() {
		Date date = new Date();
		System.out.println("ACTUALIZANDO CANCIONES");
		List<Event> events = getEventListFilterByDay(date);
		Collections.sort(events, (o1, o2) -> o1.getLocalTime().compareTo(o2.getLocalTime()));
		SimpleDateFormat localTimeFormat = new SimpleDateFormat("HH:mm:ss");

		for (Event event : events) {
			Date eventDate = event.getReproductionHour();

			// Cogemos la hora del evento para actualizar la fecha actual
			String eventTimeStr = localTimeFormat.format(eventDate);
			LocalTime time = LocalTime.parse(eventTimeStr);
			DateTime dt = DateTime.now();

			Playlist playlist = event.getPlaylist();
			// actualizamos la playlist
			playlist.setLastPlay(dt.withTime(time).toDate());
			playlistRepository.save(playlist);

			List<OrderSong> orderSongList = getOrderSongsOrderByPosition(playlist.getPlaylistId());
			LocalTime acumTime = time;
			for (OrderSong orderSong : orderSongList) {
				DateTime updatedDateTime = dt.withTime(acumTime);
				Song song = orderSong.getSong();
				song.setLastReproduction(updatedDateTime.toDate());
				songRepository.save(song);

				acumTime = acumTime.plusMillis(toIntExact(song.getDuration()));

			}

		}

	}

}
