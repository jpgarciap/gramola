package es.udc.fic.tfg.jp.service.song;

import java.text.SimpleDateFormat;

import es.udc.fic.tfg.jp.domain.song.Song;

public class SongDto {

	private Song song;
	private long likes;
	private String durationMinSec;
	private String lastReproduction;

	public SongDto(Song song, long likes, long durationSeconds, String durationMinSec) {
		super();
		this.likes = likes;
		this.song = song;
		this.durationMinSec = durationMinSec;
		if (song.getLastReproduction() == null) {
			this.lastReproduction = " ";
		} else {
			SimpleDateFormat localDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			String strDate = localDateFormat.format(song.getLastReproduction());
			this.lastReproduction = strDate;

		}
	}

	public long getLikes() {
		return likes;
	}

	public void setLikes(long likes) {
		this.likes = likes;
	}

	public String getDurationMinSec() {
		return durationMinSec;
	}

	public void setDurationMinSec(String durationMinSec) {
		this.durationMinSec = durationMinSec;
	}

	public String getLastReproduction() {
		return lastReproduction;
	}

	public void setLastReproduction(String lastReproduction) {
		this.lastReproduction = lastReproduction;
	}

	public Song getSong() {
		return song;
	}

	public void setSong(Song song) {
		this.song = song;
	}

}
