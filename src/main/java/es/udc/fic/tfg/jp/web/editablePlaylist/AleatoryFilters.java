package es.udc.fic.tfg.jp.web.editablePlaylist;

import java.util.List;

public class AleatoryFilters {

	private List<Long> genreIdList;
	private long maxSongs;
	// En al interfaz puede ser none/likes/lastReproduction
	private String top;
	private String localDate;

	public AleatoryFilters() {
		super();
	}

	public AleatoryFilters(List<Long> genreIdList, int maxSongs, String top, String localDate) {
		super();
		this.genreIdList = genreIdList;
		this.maxSongs = maxSongs;
		this.top = top;
		this.localDate = localDate;
	}

	public List<Long> getGenreIdList() {
		return genreIdList;
	}

	public void setGenreIdList(List<Long> genreIdList) {
		this.genreIdList = genreIdList;
	}

	public long getMaxSongs() {
		return maxSongs;
	}

	public void setMaxSongs(long maxSongs) {
		this.maxSongs = maxSongs;
	}

	public String getTop() {
		return top;
	}

	public void setTop(String top) {
		this.top = top;
	}

	public String getLocalDate() {
		return localDate;
	}

	public void setLocalDate(String localDate) {
		this.localDate = localDate;
	}

}
