package es.udc.fic.tfg.jp.web.home;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.service.song.SongService;
import es.udc.fic.tfg.jp.service.user.UserService;

@Controller
public class DefaultController {

	@Autowired
	UserService userService;

	@Autowired
	SongService songService;

	@GetMapping("/")
	public String home1() {
		return "/home";
	}

	@GetMapping("/home")
	public String home() {
		return "/home";
	}

	@GetMapping("/user")
	public String user() {
		return "/user";
	}

	@GetMapping("/contact")
	public String about() {
		return "/contact";
	}

	@GetMapping("/403")
	public String error403() {
		return "/error/403";
	}

	@PostMapping("/user/likeSong")
	public ResponseEntity<String> likeSong() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findByEmail(authentication.getName());
		String response = songService.userLikeSong(user, new Date());
		ResponseEntity<String> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		return responseEntity;

	}

	@PostMapping("/user/disLikeSong")
	public ResponseEntity<String> disLikeSong() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findByEmail(authentication.getName());
		String response = songService.userDislikeSong(user, new Date());
		ResponseEntity<String> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		return responseEntity;
	}

}