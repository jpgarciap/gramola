package es.udc.fic.tfg.jp.domain.editableplaylist;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EditablePlaylistRepository extends JpaRepository<EditablePlaylist, Long> {

}
