package es.udc.fic.tfg.jp.web.playlist;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import es.udc.fic.tfg.jp.service.playlist.PlaylistDto;
import es.udc.fic.tfg.jp.service.playlist.PlaylistService;
import es.udc.fic.tfg.jp.service.user.UserService;

@Controller
public class PlaylistController {

	@Autowired
	PlaylistService playlistService;

	@Autowired
	UserService userService;

	@GetMapping("/admin/playlists")
	public String getPlaylists() {
		return "/admin/playlists";
	}

	@RequestMapping(value = "/admin/playlistData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<PlaylistDto> getPlaylistData() {
		List<PlaylistDto> playlistDtoList = playlistService.getAllPlaylistDto();
		return playlistDtoList;
	}

	@PostMapping("/admin/deletePlaylist")
	String deleteSong(@RequestBody long playlistId) {

		playlistService.removePlayList(playlistId);
		return "redirect:/admin/playlists";
	}

}
