package es.udc.fic.tfg.jp.web.schedule;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.udc.fic.tfg.jp.service.playlist.PlaylistService;
import es.udc.fic.tfg.jp.service.schedule.NewEventForm;
import es.udc.fic.tfg.jp.service.schedule.ScheduleService;

@Controller
public class ScheduleController {

	@Autowired
	ScheduleService scheduleService;

	@Autowired
	PlaylistService playlistService;

	@GetMapping("/admin/schedule")
	public String getSchedule(Model model) {

		NewEventForm newEventForm = new NewEventForm();

		model.addAttribute("playlists", playlistService.getAllPlaylistsOrderByName());
		model.addAttribute("events", scheduleService.getAllEvents());
		model.addAttribute("newEventForm", newEventForm);
		model.addAttribute("longWrapper", new LongWrapper());

		return "/admin/schedule";
	}

	@PostMapping("/admin/schedule/newEventForm")
	String createEvent(@ModelAttribute NewEventForm newEventForm) {

		scheduleService.newEvent(newEventForm);

		return "redirect:/admin/schedule";

	}

	@PostMapping("/admin/schedule/deleteEvent")
	String deleteEvent(@ModelAttribute LongWrapper longWrapper) {
		scheduleService.deleteEvent(longWrapper.getId());
		return "redirect:/admin/schedule";

	}

	@GetMapping("/admin/event/all")
	public @ResponseBody List<EventDto> getAllEvents() {

		return scheduleService.getAllEvents();
	}
}
