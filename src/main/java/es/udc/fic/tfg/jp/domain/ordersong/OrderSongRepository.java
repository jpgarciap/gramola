package es.udc.fic.tfg.jp.domain.ordersong;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderSongRepository extends JpaRepository<OrderSong, Long> {

}
