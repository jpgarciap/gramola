package es.udc.fic.tfg.jp.domain.playlist;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;

@Entity
@Table(name = "playlist")
public class Playlist {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long playlistId;
	@Column(unique = true)
	private String name;
	private String description;
	private String path;
	private long duration;
	private Date creationDate;
	private Date lastPlay;
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private List<OrderSong> orderSongs = new ArrayList<>();

	public Playlist() {
	}

	public Playlist(String name, String description, long duration, Date creationDate) {
		super();
		this.name = name;
		this.description = description;
		this.duration = duration;
		this.creationDate = creationDate;
	}

	public Playlist(String name, String description, String path, long duration) {
		super();
		this.name = name;
		this.description = description;
		this.path = path;
		this.creationDate = new Date();
		this.duration = duration;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setOrderSongs(List<OrderSong> orderSongs) {
		this.orderSongs = orderSongs;
	}

	public long getPlaylistId() {
		return playlistId;
	}

	public void setPlaylistId(long playlistId) {
		this.playlistId = playlistId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public Date getLastPlay() {
		return lastPlay;
	}

	public void setLastPlay(Date lastPlay) {
		this.lastPlay = lastPlay;
	}

	public List<OrderSong> getOrderSongs() {
		return orderSongs;
	}

}
