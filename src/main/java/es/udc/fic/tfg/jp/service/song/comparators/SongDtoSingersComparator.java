package es.udc.fic.tfg.jp.service.song.comparators;

import java.util.Comparator;

import es.udc.fic.tfg.jp.service.song.SongDto;

public class SongDtoSingersComparator implements Comparator<SongDto> {

	@Override
	public int compare(SongDto o1, SongDto o2) {
		return o1.getSong().getSingers().compareTo(o2.getSong().getSingers());
	}

}
