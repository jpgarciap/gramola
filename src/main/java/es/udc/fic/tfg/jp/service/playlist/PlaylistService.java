package es.udc.fic.tfg.jp.service.playlist;

import java.text.ParseException;
import java.util.List;

import es.udc.fic.tfg.jp.domain.playlist.Playlist;
import es.udc.fic.tfg.jp.web.editablePlaylist.AleatoryFilters;

public interface PlaylistService {

	void addSongsEditablePlaylist(List<Long> songIdList, long userId);

	void removeSongsEditablePlaylist(List<Long> songIdList, long userId);

	void changeOrderSongsEditablePlaylist(List<OrderSongPlDto> orderSongPlDtoList);

	void generateAleatoryEditablePlayList(AleatoryFilters aleatoryFilters, long userId) throws ParseException;

	List<OrderSongPlDto> getOrderSongDtoEditablePlaylistFromUser(long userId);

	Playlist createPlayList(String name, String description, long userId);

	Playlist updatePlayList(Playlist playlist);

	String getDurationEditablePlaylistHoursMinSeconds(long userId);

	List<PlaylistDto> getAllPlaylistDto();

	boolean removePlayList(long playlistId);

	List<Playlist> getAllPlaylistsOrderByName();

}
