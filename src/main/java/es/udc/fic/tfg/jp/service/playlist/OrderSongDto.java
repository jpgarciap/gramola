package es.udc.fic.tfg.jp.service.playlist;

import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;

public class OrderSongDto {

	private OrderSong orderSong;
	private long likes;
	private String duration;

	public OrderSongDto(OrderSong orderSong, long likes, String duration) {
		super();
		this.orderSong = orderSong;
		this.likes = likes;
		this.duration = duration;
	}

	public OrderSong getOrderSong() {
		return orderSong;
	}

	public void setOrderSong(OrderSong orderSong) {
		this.orderSong = orderSong;
	}

	public long getLikes() {
		return likes;
	}

	public void setLikes(long likes) {
		this.likes = likes;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

}
