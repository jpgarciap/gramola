package es.udc.fic.tfg.jp.service.playlist;

import java.text.SimpleDateFormat;
import java.util.List;

import es.udc.fic.tfg.jp.domain.playlist.Playlist;

public class PlaylistDto {

	private long playlistId;
	private String name;
	private String path;
	private long duration;
	private String description;
	private String creationDate;
	private String lastplay;
	private String durationHoursMinSeconds;
	private List<OrderSongPlDto> orderSongsDto;

	public PlaylistDto(Playlist playlist, String durationHoursMinSeconds, List<OrderSongPlDto> orderSong) {
		super();
		this.playlistId = playlist.getPlaylistId();
		this.name = playlist.getName();
		this.path = playlist.getPath();
		this.description = playlist.getDescription();
		this.duration = playlist.getDuration();
		SimpleDateFormat localDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		if (playlist.getCreationDate() == null) {
			this.creationDate = " ";
		} else {
			this.creationDate = localDateFormat.format(playlist.getCreationDate());
		}

		if (playlist.getLastPlay() == null) {
			this.lastplay = " ";
		} else {
			this.lastplay = localDateFormat.format(playlist.getLastPlay());
		}
		this.durationHoursMinSeconds = durationHoursMinSeconds;
		this.orderSongsDto = orderSong;
	}

	public long getPlaylistId() {
		return playlistId;
	}

	public void setPlaylistId(long playlistId) {
		this.playlistId = playlistId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String crerationDate) {
		this.creationDate = crerationDate;
	}

	public String getLastplay() {
		return lastplay;
	}

	public void setLastplay(String lastplay) {
		this.lastplay = lastplay;
	}

	public String getDurationHoursMinSeconds() {
		return durationHoursMinSeconds;
	}

	public void setDurationHoursMinSeconds(String durationHoursMinSeconds) {
		this.durationHoursMinSeconds = durationHoursMinSeconds;
	}

	public List<OrderSongPlDto> getOrderSongsPlDto() {
		return orderSongsDto;
	}

	public void setOrderSongsPlDto(List<OrderSongPlDto> orderSong) {
		this.orderSongsDto = orderSong;
	}

}
