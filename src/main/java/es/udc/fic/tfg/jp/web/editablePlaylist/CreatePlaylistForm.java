package es.udc.fic.tfg.jp.web.editablePlaylist;

import javax.validation.constraints.NotBlank;

public class CreatePlaylistForm {

	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";
	private static final String EMAIL_EXISTS_MESSAGE = "Name exists";

	@NotBlank(message = CreatePlaylistForm.NOT_BLANK_MESSAGE)
	@PlaylistNameExists(message = CreatePlaylistForm.EMAIL_EXISTS_MESSAGE)
	private String name;

	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
