package es.udc.fic.tfg.jp.domain.likedsong;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.udc.fic.tfg.jp.domain.song.Song;
import es.udc.fic.tfg.jp.domain.user.User;

@Entity
@Table(name = "likedSong")
public class LikedSong {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long likeId;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "songId")
	private Song song;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;

	private Date date;

	public LikedSong() {
	}

	public LikedSong(Song song, User user, Date date) {
		super();
		this.song = song;
		this.user = user;
		this.date = date;
	}

	public long getLikeId() {
		return likeId;
	}

	public void setLikeId(long likeId) {
		this.likeId = likeId;
	}

	public Song getSong() {
		return song;
	}

	public void setSong(Song song) {
		this.song = song;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
