package es.udc.fic.tfg.jp.web.editablePlaylist;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.springframework.stereotype.Component;

import es.udc.fic.tfg.jp.domain.playlist.PlaylistRepository;

@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = PlaylistNameExistsValidator.class)
@Documented
public @interface PlaylistNameExists {

	String message() default "Name already exists";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}

@Component
class PlaylistNameExistsValidator implements ConstraintValidator<PlaylistNameExists, String> {

	private final PlaylistRepository playlistRepository;

	public PlaylistNameExistsValidator(PlaylistRepository playlistRepository) {
		this.playlistRepository = playlistRepository;
	}

	public void initialize(PlaylistNameExists constraintAnnotation) {

	}

	public boolean isValid(String value, ConstraintValidatorContext context) {
		return !playlistRepository.existsByName(value);
	}

}