var holding = false;
var track = document.getElementById('track');
var progress = document.getElementById('progress');
var play = document.getElementById('play');
var next = document.getElementById('next');
var prev = document.getElementById('prev');
var title = document.getElementById('title');
var artist = document.getElementById('artist');
var art = document.getElementById('art');
var current_track = 0;
var tableId;
var data, audio, duration;
var divoffset = $("#div-player").offset();
var playing = false;


$(document).ready(function() {


	$('#playlistsTable tbody').on('click', 'tr tbody tr #playBtn', function () {

		var table = $(this).closest('table').DataTable();
		tableId = table.table().node().id;
		data = table.row( $(this).parents('tr') ).data();
		var rows = table.rows().data();
		current_track=  ($(this).parents('tr') ).index();
		audio.src = "private/" + data.path;
		audio.onloadeddata = function() {
			updateInfo();
		}
	} );

} );

window.addEventListener('load',init(), false);
function init() {
	audio = new Audio();
}

audio.addEventListener('timeupdate', updateTrack, false);
audio.addEventListener('loadedmetadata', function () {
	duration = this.duration;
}, false);
window.onmousemove = function (e) {
	e.preventDefault();
	if (holding) seekTrack(e);
}
window.onmouseup = function (e) {
	holding = false;
}
track.onmousedown = function (e) {
	holding = true;

}
play.onclick = function () {
	playing ? audio.pause() : audio.play();
}

audio.addEventListener("pause", function () {
	play.innerHTML = '<img class="pad" src="../../img/play.png" />';
	playing = false;
}, false);

audio.addEventListener("playing", function () {
	play.innerHTML = '<img src="../../img/pause.png" />';
	playing = true;
}, false);
next.addEventListener("click", nextTrack, false);
prev.addEventListener("click", prevTrack, false);


function updateTrack() {
	curtime = audio.currentTime;
	percent = Math.round((curtime * 100) / duration);
	progress.style.width = percent + '%';
	handler.style.left = percent + '%';
}

function seekTrack(e) {
	event = e || window.event;
	var x = e.pageX - divoffset.left - player.offsetLeft - track.offsetLeft;
	percent = Math.round((x * 100) / track.offsetWidth);
	if (percent > 100) percent = 100;
	if (percent < 0) percent = 0;
	progress.style.width = percent + '%';
	handler.style.left = percent + '%';
	audio.play();
	audio.currentTime = (percent * duration) / 100
}

function nextTrack() {
	var table = $('#' + tableId).DataTable();
	var rows = table.rows( { filter : 'applied'} ).data();
	var length = table.rows( { filter : 'applied'} ).nodes().length;
	current_track++;
	current_track = current_track % ( length);
	data = rows[current_track];
	if (typeof data !== 'undefined') {
		audio.src = "private/" + data.path;
	}
	audio.onloadeddata = function() {
		updateInfo();
	}
}
$(document).ready(nextTrack);

function prevTrack() {
	var table = $('#' + tableId).DataTable();
	var rows = table.rows( { filter : 'applied'} ).data();
	var length = table.rows( { filter : 'applied'} ).nodes().length;
	current_track--;
	current_track = (current_track == -1 ? (length - 1) : current_track);
	data = rows[current_track];
	if (typeof data !== 'undefined') {
		audio.src = "private/" + data.path;
	}
	audio.onloadeddata = function() {
		updateInfo();
	}
}

$(document).ready(prevTrack);

function updateInfo() {
	title.textContent = data.title;
	artist.textContent = data.singers;
	/*art.src = song.art; 
    art.onload = function() {
        audio.play();
    }*/

	audio.play();
}