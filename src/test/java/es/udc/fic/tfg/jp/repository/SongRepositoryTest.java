package es.udc.fic.tfg.jp.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.tfg.jp.Application;
import es.udc.fic.tfg.jp.domain.genre.Genre;
import es.udc.fic.tfg.jp.domain.genre.GenreRepository;
import es.udc.fic.tfg.jp.domain.song.Song;
import es.udc.fic.tfg.jp.domain.song.SongRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class SongRepositoryTest {

	@Autowired
	private SongRepository songRepository;

	@Autowired
	private GenreRepository genreRepository;

	private Genre rock, rap, pop, techno;

	private Song song1, song2, song3;

	@Before
	public void setUp() {
		rock = genreRepository.save(new Genre("Rock"));
		rap = genreRepository.save(new Genre("rap"));
		pop = genreRepository.save(new Genre("pop"));
		techno = genreRepository.save(new Genre("techno"));

		song1 = songRepository.save(new Song("welcome to the jungle - guns n'rose", rock, ".mp3", (long) 120,
				"welcome to the jungle", "/rock/welcome to the jungle - guns", "guns n'rose"));
		song2 = songRepository.save(new Song("californication - red hot chili peppers", rock, ".mp3", (long) 130,
				"californication", "/rock/californication - red hot", "red hot chili peppers"));
		songRepository.save(
				new Song("rap god - eminem", rap, ".mp3", (long) 120, "rap god", "/rap/rap god - eminem", "eminem"));
		songRepository.save(new Song("lose yourself - eminem", rap, ".mp3", (long) 120, "lose yoursef",
				"/rap/lose yoursel - eminem", "eminem"));
		song3 = songRepository
				.save(new Song("loba - shakira", pop, ".mp3", (long) 120, "loba", "/pop/loba - shakira", "shakira"));
		songRepository.save(new Song("we are alive - loco dice", techno, ".mp3", (long) 120, "we are alive",
				"/techno/we are alive", "loco dice"));
		songRepository.save(new Song("kalif storch - modeselektor", techno, ".mp3", (long) 120, "kalif storch",
				"/techno/kalif storch", "modeselektor"));
		songRepository
				.save(new Song("insane - marst", techno, ".mp3", (long) 120, "insane", "/techno/insane", "marst"));

	}

	@Test
	public void findByGenreTest() {
		List<Song> resultList = new ArrayList<>();
		resultList = songRepository.findByGenre(rock.getGenreId());
		assertEquals(2, resultList.size());
		resultList = songRepository.findByGenre(techno.getGenreId());
		assertEquals(3, resultList.size());
	}

	@Test
	public void existByPathTest() {
		assertTrue(songRepository.existsByPath("/rock/californication - red hot"));
		assertFalse(songRepository.existsByPath("/rock/no existe"));
	}

	@Test
	public void existsByTitleAndSingersTest() {
		assertTrue(songRepository.existsByTitleAndSingers("loba", "shakira"));
		assertFalse(songRepository.existsByTitleAndSingers("no", "existe"));
	}

	@Test
	public void findByNameTest() {
		assertNotNull(songRepository.findByName("rap god - eminem"));
		assertNull(songRepository.findByName("no - existe"));
	}

	@Test
	public void findByPathTest() {
		assertNotNull(songRepository.findByPath("/rock/californication - red hot"));
		assertNull(songRepository.findByPath("/rock/no existe"));
	}

	@Test
	public void getMaxCountTest() {
		song1.setCount(1);
		song2.setCount(4);
		song1 = songRepository.save(song1);
		song2 = songRepository.save(song2);

		assertEquals(4, songRepository.getMaxCount());

	}

	@Test
	public void deleteByCountTest() {
		song1.setCount(1);
		song2.setCount(4);
		song1 = songRepository.save(song1);
		song2 = songRepository.save(song2);
		songRepository.deleteByCount(1);
		assertFalse(songRepository.existsByPath(song1.getPath()));
	}

	@Test
	public void findByKeywords() {
		List<Song> result = new ArrayList<>();
		String keywords = "lo";
		result = songRepository.findByKeywords(new HashSet<String>(Arrays.asList(keywords.split(" "))));
		assertEquals(3, result.size());

		keywords = "sha lo";
		result = songRepository.findByKeywords(new HashSet<String>(Arrays.asList(keywords.split(" "))));
		assertEquals(song3, result.get(0));
	}

	@Test
	public void findByKeywordsAndGenre() {
		List<Song> result = new ArrayList<>();
		String keywords = "lo";
		result = songRepository.findByKeywordsAndGenre((new HashSet<String>(Arrays.asList(keywords.split(" ")))),
				pop.getGenreId());
		assertEquals(song3, result.get(0));
	}

}
