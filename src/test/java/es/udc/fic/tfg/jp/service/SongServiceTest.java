package es.udc.fic.tfg.jp.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.tfg.jp.Application;
import es.udc.fic.tfg.jp.domain.dislikedsong.DislikedSong;
import es.udc.fic.tfg.jp.domain.dislikedsong.DislikedSongRepository;
import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylist;
import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylistRepository;
import es.udc.fic.tfg.jp.domain.genre.Genre;
import es.udc.fic.tfg.jp.domain.genre.GenreRepository;
import es.udc.fic.tfg.jp.domain.likedsong.LikedSong;
import es.udc.fic.tfg.jp.domain.likedsong.LikedSongRepository;
import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;
import es.udc.fic.tfg.jp.domain.playlist.Playlist;
import es.udc.fic.tfg.jp.domain.song.Song;
import es.udc.fic.tfg.jp.domain.song.SongRepository;
import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.service.playlist.PlaylistService;
import es.udc.fic.tfg.jp.service.schedule.NewEventForm;
import es.udc.fic.tfg.jp.service.schedule.ScheduleService;
import es.udc.fic.tfg.jp.service.song.SongDto;
import es.udc.fic.tfg.jp.service.song.SongService;
import es.udc.fic.tfg.jp.service.user.UserService;
import es.udc.fic.tfg.jp.web.song.FindSongForm;
import es.udc.fic.tfg.jp.web.support.Pageable;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class SongServiceTest {

	@Autowired
	private SongService songService;

	@Autowired
	private SongRepository songRepository;

	@Autowired
	private GenreRepository genreRepository;

	@Autowired
	private LikedSongRepository likedSongRepository;

	@Autowired
	private DislikedSongRepository dislikedSongRepository;

	@Autowired
	private PlaylistService playlistService;

	@Autowired
	private EditablePlaylistRepository editablePlaylistRepository;

	@Autowired
	private ScheduleService scheduleService;

	@Autowired
	private UserService userService;

	private Genre rock, rap, pop, techno;
	private Song song1, song2, song3, song4;
	private User user, user2, user3, admin;

	@Before
	public void setUp() {

		rock = genreRepository.save(new Genre("Rock"));
		rap = genreRepository.save(new Genre("Rap"));
		pop = genreRepository.save(new Genre("Pop"));
		techno = genreRepository.save(new Genre("Techno"));

		song1 = songRepository.save(new Song("welcome to the jungle - guns n'rose", rock, ".mp3", (long) 120000,
				"welcome to the jungle", "guns n'rose"));
		song2 = songRepository.save(new Song("californication - red hot chili peppers", rock, ".mp3", (long) 130000,
				"californication", "red hot chili peppers"));
		song3 = songRepository.save(new Song("rap god - eminem", rap, ".mp3", (long) 120000, "rap god", "eminem"));
		song4 = songRepository
				.save(new Song("lose yourself - eminem", rap, ".mp3", (long) 120, "lose yoursef", "eminem"));
		songRepository.save(new Song("loba - shakira", pop, ".mp3", (long) 120, "loba", "shakira"));
		songRepository
				.save(new Song("we are alive - loco dice", techno, ".mp3", (long) 120, "we are alive", "loco dice"));
		songRepository.save(
				new Song("kalif storch - modeselektor", techno, ".mp3", (long) 120, "kalif storch", "modeselektor"));
		songRepository.save(new Song("insane - marst", techno, ".mp3", (long) 120, "insane", "marst"));
		user = userService.registerUser(new User("udc@udc.es", "pass", "user", "udc"));
		user2 = userService.registerUser(new User("udc2@udc.es", "pass", "user2", "udc"));
		user3 = userService.registerUser(new User("udc3@udc.es", "pass", "user2", "udc"));

		admin = userService.registerUser(new User("admin@udc.es", "admin", "user", "udc"));
		EditablePlaylist editablePlaylist = new EditablePlaylist(admin, (long) 0, new ArrayList<OrderSong>());
		admin.setEditablePlaylist(editablePlaylist);
		editablePlaylistRepository.save(editablePlaylist);

	}

	@Test
	public void userLikeSongTest() throws ParseException {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song3.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("nombre", "desc", admin.getUserId());
		List<Playlist> playlists = playlistService.getAllPlaylistsOrderByName();
		NewEventForm newEventForm = new NewEventForm(3, "10:00:00", playlists.get(0).getPlaylistId());
		scheduleService.newEvent(newEventForm);

		String dateLikeStr = "10/10/2018 10:01:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = sdf.parse(dateLikeStr);

		String result = songService.userLikeSong(admin, date);
		assertEquals(song1.getTitle(), result);
	}

	@Test
	public void userLikeSong2Test() throws ParseException {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song3.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("nombre", "desc", admin.getUserId());
		List<Playlist> playlists = playlistService.getAllPlaylistsOrderByName();
		NewEventForm newEventForm = new NewEventForm(3, "10:00:00", playlists.get(0).getPlaylistId());
		scheduleService.newEvent(newEventForm);

		String dateLikeStr = "10/10/2018 10:01:00";
		String dateSecondLikeStr = "10/10/2018 10:01:30";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = sdf.parse(dateLikeStr);

		songService.userLikeSong(user, date);
		assertEquals(1, likedSongRepository.countBySongAndDate(song1,
				new LocalDate(sdf.parse(dateLikeStr)).minusMonths(2).toDate()));

		// El mismo usuario le vuelve a dar me gusta a la misma canción
		songService.userLikeSong(user, sdf.parse(dateSecondLikeStr));
		assertEquals(1, likedSongRepository.countBySongAndDate(song1,
				new LocalDate(sdf.parse(dateLikeStr)).minusMonths(2).toDate()));
	}

	@Test
	public void findAllGenreOrderByNameTest() {
		List<Genre> resultList = songService.getAllGenreOrderByName();
		assertEquals(resultList.size(), 4);
		assertEquals(resultList.get(0), pop);
		assertEquals(resultList.get(1), rap);
		assertEquals(resultList.get(2), rock);
		assertEquals(resultList.get(3), techno);
	}

	@Test
	public void likeAndDislikeSongTest() throws ParseException {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song3.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("nombre", "desc", admin.getUserId());
		List<Playlist> playlists = playlistService.getAllPlaylistsOrderByName();
		NewEventForm newEventForm = new NewEventForm(3, "10:00:00", playlists.get(0).getPlaylistId());
		scheduleService.newEvent(newEventForm);
		String dateLikeStr = "10/10/2018 10:02:30";
		String dateDisLikeStr = "10/10/2018 10:03:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		songService.userLikeSong(user, sdf.parse(dateLikeStr));
		LikedSong likedSong = likedSongRepository.findByUserAndSong(user, song2);
		assertNotNull(likedSong);
		songService.userDislikeSong(user, sdf.parse(dateDisLikeStr));
		DislikedSong dislikedSong = dislikedSongRepository.findByUserAndSong(user, song2);
		assertNotNull(dislikedSong);
		// Comprobamos que al darle dislike, el like dado con anterioridad se borra de
		// nuestra BD
		likedSong = likedSongRepository.findByUserAndSong(user, song2);
		assertNull(likedSong);
	}

	@Test
	public void twiceDislikeTest() throws ParseException {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song3.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("nombre", "desc", admin.getUserId());
		List<Playlist> playlists = playlistService.getAllPlaylistsOrderByName();
		NewEventForm newEventForm = new NewEventForm(3, "10:00:00", playlists.get(0).getPlaylistId());
		scheduleService.newEvent(newEventForm);

		String dateDisLikeStr = "10/10/2018 10:02:30";
		String dateDisLike2Str = "10/10/2018 10:03:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		songService.userDislikeSong(user, sdf.parse(dateDisLikeStr));
		songService.userDislikeSong(user, sdf.parse(dateDisLike2Str));
		DislikedSong second = dislikedSongRepository.findByUserAndSong(user, song2);
		assertTrue(second.getDate().equals(dislikedSongRepository.findByUserAndSong(user, song2).getDate()));
	}

	@Test
	public void twiceUserlikeSongTest() throws ParseException {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song3.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("nombre", "desc", admin.getUserId());
		List<Playlist> playlists = playlistService.getAllPlaylistsOrderByName();
		NewEventForm newEventForm = new NewEventForm(3, "10:00:00", playlists.get(0).getPlaylistId());
		scheduleService.newEvent(newEventForm);

		String dateLikeStr = "10/10/2018 10:02:30";
		String dateLike2Str = "10/10/2018 10:03:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		songService.userLikeSong(user, sdf.parse(dateLikeStr));
		songService.userLikeSong(user, sdf.parse(dateLike2Str));
		LikedSong second = likedSongRepository.findByUserAndSong(user, song2);
		assertTrue(second.getDate().equals(likedSongRepository.findByUserAndSong(user, song2).getDate()));
	}

	@Test
	public void dislikeAndLikeSongTest() throws ParseException {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song3.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("nombre", "desc", admin.getUserId());
		List<Playlist> playlists = playlistService.getAllPlaylistsOrderByName();
		NewEventForm newEventForm = new NewEventForm(3, "10:00:00", playlists.get(0).getPlaylistId());
		scheduleService.newEvent(newEventForm);

		String dateDisLikeStr = "10/10/2018 10:02:30";
		String dateLikeStr = "10/10/2018 10:03:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		songService.userDislikeSong(user, sdf.parse(dateDisLikeStr));
		DislikedSong dislikedSong = dislikedSongRepository.findByUserAndSong(user, song2);
		assertNotNull(dislikedSong);
		songService.userLikeSong(user, sdf.parse(dateLikeStr));
		LikedSong likedSong = likedSongRepository.findByUserAndSong(user, song2);
		assertNotNull(likedSong);
		// Comprobamos que al darle dislike, el like dado con anterioridad se borra de
		// nuestra BD
		dislikedSong = dislikedSongRepository.findByUserAndSong(user, song3);
		assertNull(dislikedSong);
	}

	@Test
	public void findAllSongsWithoutFiltersTest() {
		FindSongForm filters = new FindSongForm("", LocalDate.now(), "title", -1, true);
		Pageable<SongDto> pages = songService.findSongs(filters, PageRequest.of(0, 5));
		assertEquals(5, pages.getListForPage().size());
		assertEquals(2, pages.getMaxPages());
	}

	@Test
	public void totalLikesSongPositiveTest() throws ParseException {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song3.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("nombre", "desc", admin.getUserId());
		List<Playlist> playlists = playlistService.getAllPlaylistsOrderByName();
		NewEventForm newEventForm = new NewEventForm(3, "10:00:00", playlists.get(0).getPlaylistId());
		scheduleService.newEvent(newEventForm);

		String dateDisLikeStr = "10/10/2018 10:02:30";
		String dateLikeStr = "10/10/2018 10:03:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		// a 2 usuarios les gusta la canción y a 1 no le gusta, total = 1
		songService.userLikeSong(user, sdf.parse(dateLikeStr));
		songService.userLikeSong(user2, sdf.parse(dateLikeStr));
		songService.userDislikeSong(user3, sdf.parse(dateDisLikeStr));

		FindSongForm filters = new FindSongForm(song2.getTitle(),
				new LocalDate(sdf.parse(dateDisLikeStr)).minusMonths(2), "title", -1, true);
		Pageable<SongDto> pages = songService.findSongs(filters, PageRequest.of(0, 5));
		assertEquals(1, pages.getList().get(0).getLikes());

	}

	@Test
	public void totalLikesSongNegativeTest() throws ParseException {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song3.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("nombre", "desc", admin.getUserId());
		List<Playlist> playlists = playlistService.getAllPlaylistsOrderByName();
		NewEventForm newEventForm = new NewEventForm(3, "10:00:00", playlists.get(0).getPlaylistId());
		scheduleService.newEvent(newEventForm);

		String dateDisLikeStr = "10/10/2018 10:02:30";
		String dateLikeStr = "10/10/2018 10:03:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		// a 2 usuarios les gusta la canción y a 1 no le gusta, total = 1
		songService.userLikeSong(user, sdf.parse(dateLikeStr));
		songService.userDislikeSong(user2, sdf.parse(dateLikeStr));
		songService.userDislikeSong(user3, sdf.parse(dateDisLikeStr));

		FindSongForm filters = new FindSongForm(song2.getTitle(),
				new LocalDate(sdf.parse(dateDisLikeStr)).minusMonths(2), "Title", -1, true);
		Pageable<SongDto> pages = songService.findSongs(filters, PageRequest.of(0, 5));
		assertEquals(-1, pages.getList().get(0).getLikes());
	}

	@Test
	public void findSongsByGenreOrderByTitleAscTest() throws ParseException {
		String dateDisLikeStr = "10/10/2018 10:02:30";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		FindSongForm filters = new FindSongForm("", new LocalDate(sdf.parse(dateDisLikeStr)).minusMonths(2), "Title",
				rock.getGenreId(), true);
		Pageable<SongDto> pages = songService.findSongs(filters, PageRequest.of(0, 5));

		// Nos encuentra 2 canciones
		assertEquals(1, pages.getMaxPages());
		List<SongDto> resultList = pages.getListForPage();
		assertEquals(resultList.get(0).getSong(), song1);
		assertEquals(resultList.get(1).getSong(), song2);
	}

	@Test
	public void findSongsByGenreOrderByTitleDescTest() throws ParseException {
		String dateDisLikeStr = "10/10/2018 10:02:30";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		FindSongForm filters = new FindSongForm("", new LocalDate(sdf.parse(dateDisLikeStr)).minusMonths(2), "Title",
				rap.getGenreId(), false);
		Pageable<SongDto> pages = songService.findSongs(filters, PageRequest.of(0, 5));

		// Nos encuentra 2 canciones
		assertEquals(1, pages.getMaxPages());
		List<SongDto> resultList = pages.getListForPage();
		assertEquals(resultList.get(0).getSong(), song4);
		assertEquals(resultList.get(1).getSong(), song3);
	}

	@Test
	public void findAllSongsOrderByLikesDesc() throws ParseException {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song3.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("nombre", "desc", admin.getUserId());
		List<Playlist> playlists = playlistService.getAllPlaylistsOrderByName();
		NewEventForm newEventForm = new NewEventForm(3, "10:00:00", playlists.get(0).getPlaylistId());
		scheduleService.newEvent(newEventForm);

		String dateLikeSong2 = "10/10/2018 10:02:30";
		String dateLikeSong3 = "10/10/2018 10:05:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		// el user le da me gusta a la cancion 2 y 3, el user2 le da a la 2
		songService.userLikeSong(user, sdf.parse(dateLikeSong2));
		songService.userLikeSong(user2, sdf.parse(dateLikeSong2));
		songService.userLikeSong(user, sdf.parse(dateLikeSong3));

		FindSongForm filters = new FindSongForm("", new LocalDate(sdf.parse(dateLikeSong2)).minusMonths(2), "Likes", -1,
				false);

		Pageable<SongDto> pages = songService.findSongs(filters, PageRequest.of(0, 5));
		assertEquals(2, pages.getMaxPages());
		List<SongDto> resultList = pages.getList();
		assertEquals(resultList.get(0).getSong(), song2);
		assertEquals(resultList.get(1).getSong(), song3);
	}

	@Test
	public void findAllSongsOrderByLikesAsc() throws ParseException {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song3.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("nombre", "desc", admin.getUserId());
		List<Playlist> playlists = playlistService.getAllPlaylistsOrderByName();
		NewEventForm newEventForm = new NewEventForm(3, "10:00:00", playlists.get(0).getPlaylistId());
		scheduleService.newEvent(newEventForm);

		String dateLikeSong2 = "10/10/2018 10:02:30";
		String dateLikeSong3 = "10/10/2018 10:05:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		// el user le da me gusta a la cancion 2 y 3, el user2 le da a la 2
		songService.userLikeSong(user, sdf.parse(dateLikeSong2));
		songService.userLikeSong(user2, sdf.parse(dateLikeSong2));
		songService.userLikeSong(user, sdf.parse(dateLikeSong3));

		FindSongForm filters = new FindSongForm("", new LocalDate(sdf.parse(dateLikeSong2)).minusMonths(2), "Likes", -1,
				true);

		Pageable<SongDto> pages = songService.findSongs(filters, PageRequest.of(0, 5));
		assertEquals(2, pages.getMaxPages());
		List<SongDto> resultList = pages.getList();
		assertEquals(resultList.get(resultList.size() - 1).getSong(), song2);
		assertEquals(resultList.get(resultList.size() - 2).getSong(), song3);
	}

}
