package es.udc.fic.tfg.jp.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.tfg.jp.Application;
import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylist;
import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylistRepository;
import es.udc.fic.tfg.jp.domain.event.Event;
import es.udc.fic.tfg.jp.domain.event.EventRepository;
import es.udc.fic.tfg.jp.domain.genre.Genre;
import es.udc.fic.tfg.jp.domain.genre.GenreRepository;
import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;
import es.udc.fic.tfg.jp.domain.playlist.Playlist;
import es.udc.fic.tfg.jp.domain.song.Song;
import es.udc.fic.tfg.jp.domain.song.SongRepository;
import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.service.playlist.PlaylistService;
import es.udc.fic.tfg.jp.service.schedule.NewEventForm;
import es.udc.fic.tfg.jp.service.schedule.ScheduleService;
import es.udc.fic.tfg.jp.service.user.UserService;
import es.udc.fic.tfg.jp.web.schedule.EventDto;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class ScheduleServiceTest {

	@Autowired
	ScheduleService scheduleService;

	@Autowired
	EventRepository eventRepository;

	@Autowired
	SongRepository songRepository;

	@Autowired
	PlaylistService playlistService;

	@Autowired
	UserService userService;

	@Autowired
	EditablePlaylistRepository editablePlaylistRepository;

	@Autowired
	GenreRepository genreRepository;

	private Genre rock, rap, pop, techno;
	private Song song6, song7, song8;
	private User admin;

	@Before
	public void setUp() {

		rock = genreRepository.save(new Genre("Rock"));
		rap = genreRepository.save(new Genre("Rap"));
		pop = genreRepository.save(new Genre("Pop"));
		techno = genreRepository.save(new Genre("Techno"));

		songRepository.save(new Song("welcome to the jungle - guns n'rose", rock, ".mp3", (long) 120,
				"welcome to the jungle", "guns n'rose"));
		songRepository.save(new Song("californication - red hot chili peppers", rock, ".mp3", (long) 130,
				"californication", "red hot chili peppers"));
		songRepository.save(new Song("rap god - eminem", rap, ".mp3", (long) 120, "rap god", "eminem"));
		songRepository.save(new Song("lose yourself - eminem", rap, ".mp3", (long) 120, "lose yoursef", "eminem"));
		songRepository.save(new Song("loba - shakira", pop, ".mp3", (long) 120, "loba", "shakira"));
		song6 = songRepository
				.save(new Song("we are alive - loco dice", techno, ".mp3", (long) 120, "we are alive", "loco dice"));
		song7 = songRepository.save(
				new Song("kalif storch - modeselektor", techno, ".mp3", (long) 120, "kalif storch", "modeselektor"));
		song8 = songRepository.save(new Song("insane - marst", techno, ".mp3", (long) 120, "insane", "marst"));
		admin = userService.registerUser(new User("admin@udc.es", "admin", "user", "udc"));
		EditablePlaylist editablePlaylist = new EditablePlaylist(admin, (long) 0, new ArrayList<OrderSong>());
		admin.setEditablePlaylist(editablePlaylist);
		editablePlaylistRepository.save(editablePlaylist);
	}

	@Test
	public void newEventTest() {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song7.getSongId());
		songIdList.add(song8.getSongId());
		songIdList.add(song6.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());

		Playlist playlist = playlistService.createPlayList("playlist1", "musica variada", admin.getUserId());

		NewEventForm newEventForm = new NewEventForm(3, "13:40:00", playlist.getPlaylistId());

		Event event = scheduleService.newEvent(newEventForm);

		assertTrue(eventRepository.existsById(event.getEventId()));

	}

	@Test
	public void deleteEventTest() {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song7.getSongId());
		songIdList.add(song8.getSongId());
		songIdList.add(song6.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());

		Playlist playlist = playlistService.createPlayList("playlist1", "musica variada", admin.getUserId());

		NewEventForm newEventForm = new NewEventForm(3, "13:40:00", playlist.getPlaylistId());
		Event event = scheduleService.newEvent(newEventForm);

		assertNotNull(eventRepository.getOne(event.getEventId()));

		scheduleService.deleteEvent(event.getEventId());

		assertFalse(eventRepository.existsById(event.getEventId()));

	}

	@Test
	public void getAllEventsTest() {
		// creamos 2 eventos
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song7.getSongId());
		songIdList.add(song8.getSongId());
		songIdList.add(song6.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());

		Playlist playlist = playlistService.createPlayList("playlist1", "musica variada", admin.getUserId());

		NewEventForm newEventForm = new NewEventForm(3, "13:40:00", playlist.getPlaylistId());
		scheduleService.newEvent(newEventForm);
		songIdList = new ArrayList<>();
		songIdList.add(song7.getSongId());
		songIdList.add(song8.getSongId());
		songIdList.add(song6.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());

		Playlist playlist2 = playlistService.createPlayList("playlist2", "musica variada", admin.getUserId());

		NewEventForm newEventForm2 = new NewEventForm(3, "18:40:00", playlist2.getPlaylistId());
		scheduleService.newEvent(newEventForm2);

		List<EventDto> resultList = scheduleService.getAllEvents();

		assertEquals(2, resultList.size());

	}

}
