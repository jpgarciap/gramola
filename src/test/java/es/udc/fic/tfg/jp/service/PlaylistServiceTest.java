package es.udc.fic.tfg.jp.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.tfg.jp.Application;
import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylist;
import es.udc.fic.tfg.jp.domain.editableplaylist.EditablePlaylistRepository;
import es.udc.fic.tfg.jp.domain.genre.Genre;
import es.udc.fic.tfg.jp.domain.genre.GenreRepository;
import es.udc.fic.tfg.jp.domain.likedsong.LikedSong;
import es.udc.fic.tfg.jp.domain.likedsong.LikedSongRepository;
import es.udc.fic.tfg.jp.domain.ordersong.OrderSong;
import es.udc.fic.tfg.jp.domain.playlist.Playlist;
import es.udc.fic.tfg.jp.domain.playlist.PlaylistRepository;
import es.udc.fic.tfg.jp.domain.song.Song;
import es.udc.fic.tfg.jp.domain.song.SongRepository;
import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.service.playlist.OrderSongPlDto;
import es.udc.fic.tfg.jp.service.playlist.PlaylistDto;
import es.udc.fic.tfg.jp.service.playlist.PlaylistService;
import es.udc.fic.tfg.jp.service.song.SongService;
import es.udc.fic.tfg.jp.service.user.UserService;
import es.udc.fic.tfg.jp.web.editablePlaylist.AleatoryFilters;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class PlaylistServiceTest {

	@Autowired
	UserService userService;

	@Autowired
	GenreRepository genreRepository;

	@Autowired
	SongRepository songRepository;

	@Autowired
	LikedSongRepository likedSongRepository;

	@Autowired
	SongService songService;

	@Autowired
	PlaylistService playlistService;

	@Autowired
	PlaylistRepository playlistRepository;

	@Autowired
	EditablePlaylistRepository editablePlaylistRepository;

	private Genre rock, rap, pop, techno;
	private Song song1, song2, song3, song4, song5, song6, song7, song8;
	private User admin, user1, user2, user3, user4;

	@Before
	public void setUp() {

		rock = genreRepository.save(new Genre("Rock"));
		rap = genreRepository.save(new Genre("Rap"));
		pop = genreRepository.save(new Genre("Pop"));
		techno = genreRepository.save(new Genre("Techno"));

		song1 = songRepository.save(new Song("welcome to the jungle - guns n'rose", rock, ".mp3", (long) 120000,
				"welcome to the jungle", "guns n'rose"));
		song2 = songRepository.save(new Song("californication - red hot chili peppers", rock, ".mp3", (long) 130000,
				"californication", "red hot chili peppers"));
		song3 = songRepository.save(new Song("rap god - eminem", rap, ".mp3", (long) 120, "rap god", "eminem"));
		song4 = songRepository
				.save(new Song("lose yourself - eminem", rap, ".mp3", (long) 120000, "lose yoursef", "eminem"));
		song5 = songRepository.save(new Song("loba - shakira", pop, ".mp3", (long) 120, "loba", "shakira"));
		song6 = songRepository
				.save(new Song("we are alive - loco dice", techno, ".mp3", (long) 120, "we are alive", "loco dice"));
		song7 = songRepository.save(
				new Song("kalif storch - modeselektor", techno, ".mp3", (long) 120, "kalif storch", "modeselektor"));
		song8 = songRepository.save(new Song("insane - marst", techno, ".mp3", (long) 120, "insane", "marst"));
		admin = userService.registerUser(new User("admin@udc.es", "admin", "user", "udc"));
		EditablePlaylist editablePlaylist = new EditablePlaylist(admin, (long) 0, new ArrayList<OrderSong>());
		admin.setEditablePlaylist(editablePlaylist);
		editablePlaylistRepository.save(editablePlaylist);

		user1 = userService.registerUser(new User("udc1@udc.es", "pass", "user1", "udc"));
		user2 = userService.registerUser(new User("udc2@udc.es", "pass", "user2", "udc"));
		user3 = userService.registerUser(new User("udc3@udc.es", "pass", "user3", "udc"));
		user4 = userService.registerUser(new User("udc4@udc.es", "pass", "user4", "udc"));

	}

	@Test
	public void addSongsEditablePlayListTest() {
		EditablePlaylist editablePlaylist = admin.getEditablePlaylist();
		assertEquals(0, editablePlaylist.getOrderSongs().size());
		assertEquals(0, editablePlaylist.getDuration());
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song4.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		assertEquals(3, admin.getEditablePlaylist().getOrderSongs().size());
		assertEquals(song1.getDuration() + song2.getDuration() + song4.getDuration(),
				admin.getEditablePlaylist().getDuration());
	}

	@Test
	public void removeSongsEditablePlaylistTest() {
		EditablePlaylist editablePlaylist = admin.getEditablePlaylist();
		assertEquals(0, editablePlaylist.getOrderSongs().size());
		List<Long> songIdList = new ArrayList<>();
		List<Long> orderSongIdList = new ArrayList<>();
		List<OrderSongPlDto> orderSongDtoList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song4.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		orderSongDtoList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		assertEquals(3, orderSongDtoList.size());
		// Borramos la canción 1 y 4
		orderSongIdList.add(orderSongDtoList.get(0).getOrderSongId());
		orderSongIdList.add(orderSongDtoList.get(2).getOrderSongId());
		playlistService.removeSongsEditablePlaylist(orderSongIdList, admin.getUserId());
		orderSongDtoList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		assertEquals(1, orderSongDtoList.size());
		assertEquals(song2.getTitle(), orderSongDtoList.get(0).getTitle());
		// comprobamos que la posición se actualiza
		assertEquals(1, orderSongDtoList.get(0).getPosition());
	}

	@Test
	public void getOrderSongDtoEditablePlaylistFromUserTest() {
		EditablePlaylist editablePlaylist = admin.getEditablePlaylist();
		assertEquals(0, editablePlaylist.getOrderSongs().size());
		List<Long> songIdList = new ArrayList<>();
		List<OrderSongPlDto> orderSongDtoList = new ArrayList<>();
		songIdList.add(song5.getSongId());
		songIdList.add(song3.getSongId());
		songIdList.add(song7.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		orderSongDtoList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		assertEquals(3, orderSongDtoList.size());
		assertEquals(song5.getTitle(), orderSongDtoList.get(0).getTitle());
		assertEquals(song3.getTitle(), orderSongDtoList.get(1).getTitle());
		assertEquals(song7.getTitle(), orderSongDtoList.get(2).getTitle());
	}

	// Desplazamos una canción hacia delante
	@Test
	public void changeOrderSongEditablePlaylistTest() {
		EditablePlaylist editablePlaylist = admin.getEditablePlaylist();
		assertEquals(0, editablePlaylist.getOrderSongs().size());
		List<Long> songIdList = new ArrayList<>();
		List<OrderSongPlDto> orderSongDtoList = new ArrayList<>();
		songIdList.add(song3.getSongId());
		songIdList.add(song5.getSongId());
		songIdList.add(song6.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		assertEquals(3, admin.getEditablePlaylist().getOrderSongs().size());
		orderSongDtoList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		OrderSongPlDto primero = orderSongDtoList.get(0);
		OrderSongPlDto segundo = orderSongDtoList.get(1);
		OrderSongPlDto tercero = orderSongDtoList.get(2);
		// Desplazamos la primera posición al final
		orderSongDtoList.set(0, segundo);
		orderSongDtoList.set(1, tercero);
		orderSongDtoList.set(2, primero);

		playlistService.changeOrderSongsEditablePlaylist(orderSongDtoList);
		orderSongDtoList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		assertEquals(song5.getTitle(), orderSongDtoList.get(0).getTitle());
		assertEquals(song6.getTitle(), orderSongDtoList.get(1).getTitle());
		assertEquals(song3.getTitle(), orderSongDtoList.get(2).getTitle());
	}

	// Desplazamos una canción hacia atrás
	@Test
	public void changeOrderSongEditablePlaylistTest2() {
		EditablePlaylist editablePlaylist = admin.getEditablePlaylist();
		assertEquals(0, editablePlaylist.getOrderSongs().size());
		List<Long> songIdList = new ArrayList<>();
		List<OrderSongPlDto> orderSongDtoList = new ArrayList<>();
		songIdList.add(song3.getSongId());
		songIdList.add(song5.getSongId());
		songIdList.add(song6.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		assertEquals(3, admin.getEditablePlaylist().getOrderSongs().size());
		orderSongDtoList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		OrderSongPlDto primero = orderSongDtoList.get(0);
		OrderSongPlDto segundo = orderSongDtoList.get(1);
		OrderSongPlDto tercero = orderSongDtoList.get(2);
		// Desplazamos la primera posición al final
		orderSongDtoList.set(0, tercero);
		orderSongDtoList.set(1, primero);
		orderSongDtoList.set(2, segundo);

		playlistService.changeOrderSongsEditablePlaylist(orderSongDtoList);
		orderSongDtoList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		assertEquals(song6.getTitle(), orderSongDtoList.get(0).getTitle());
		assertEquals(song3.getTitle(), orderSongDtoList.get(1).getTitle());
		assertEquals(song5.getTitle(), orderSongDtoList.get(2).getTitle());
	}

	@Test
	public void generateAleatoryEditablePlayListAllCategoriesTest() throws ParseException {
		List<Long> genreIdList = new ArrayList<>();
		List<OrderSongPlDto> orderSongDtoPlList = new ArrayList<>();

		genreIdList.add((long) -1);
		AleatoryFilters aleatoryFilters = new AleatoryFilters(genreIdList, 4, "none",
				LocalDate.now().minusMonths(1).toString());
		playlistService.generateAleatoryEditablePlayList(aleatoryFilters, admin.getUserId());
		orderSongDtoPlList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		assertEquals(4, orderSongDtoPlList.size());
	}

	@Test
	public void generateAleatoryEditablePlaylistTwoCategoriesTest() throws ParseException {
		List<Long> genreIdList = new ArrayList<>();
		genreIdList.add(rock.getGenreId());
		genreIdList.add(rap.getGenreId());
		AleatoryFilters aleatoryFilters = new AleatoryFilters(genreIdList, 4, "none",
				LocalDate.now().minusMonths(1).toString());
		playlistService.generateAleatoryEditablePlayList(aleatoryFilters, admin.getUserId());
		EditablePlaylist editablePlaylist = admin.getEditablePlaylist();
		assertEquals(4, editablePlaylist.getOrderSongs().size());
	}

	@Test
	public void generateAleatoryEditablePlayListAllGenresOrderByLikesTest() throws ParseException {
		List<OrderSongPlDto> orderSongDtoList = new ArrayList<>();
		List<Long> genreIdList = new ArrayList<>();
		genreIdList.add((long) -1);
		AleatoryFilters aleatoryFilters = new AleatoryFilters(genreIdList, 4, "likes",
				LocalDate.now().minusMonths(1).toString());

		likedSongRepository.save(new LikedSong(song8, user1, new Date()));
		likedSongRepository.save(new LikedSong(song8, user2, new Date()));
		likedSongRepository.save(new LikedSong(song8, user3, new Date()));
		likedSongRepository.save(new LikedSong(song8, user4, new Date()));
		likedSongRepository.save(new LikedSong(song7, user1, new Date()));
		likedSongRepository.save(new LikedSong(song7, user2, new Date()));
		likedSongRepository.save(new LikedSong(song7, user3, new Date()));
		likedSongRepository.save(new LikedSong(song2, user1, new Date()));
		likedSongRepository.save(new LikedSong(song2, user4, new Date()));
		likedSongRepository.save(new LikedSong(song5, user1, new Date()));

		// song8 4 likes, song7 3 likes, song2 2 likes, song5 1 like
		playlistService.generateAleatoryEditablePlayList(aleatoryFilters, admin.getUserId());
		orderSongDtoList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		assertEquals(4, orderSongDtoList.size());
		assertEquals(song8.getTitle(), orderSongDtoList.get(0).getTitle());
		assertEquals(song7.getTitle(), orderSongDtoList.get(1).getTitle());
		assertEquals(song2.getTitle(), orderSongDtoList.get(2).getTitle());
		assertEquals(song5.getTitle(), orderSongDtoList.get(3).getTitle());
	}

	@Test
	public void generateAleatoryEditablePlaylistTwoCategoriesOrderByLikesTest() throws ParseException {
		List<OrderSongPlDto> orderSongDtoList = new ArrayList<>();
		List<Long> genreIdList = new ArrayList<>();
		genreIdList.add(rock.getGenreId());
		genreIdList.add(rap.getGenreId());

		likedSongRepository.save(new LikedSong(song1, user1, new Date()));
		likedSongRepository.save(new LikedSong(song1, user2, new Date()));
		likedSongRepository.save(new LikedSong(song2, user1, new Date()));
		likedSongRepository.save(new LikedSong(song3, user4, new Date()));
		// song1 2 likes, song2 1 like, song3 1 like, song4 0 likes
		AleatoryFilters aleatoryFilters = new AleatoryFilters(genreIdList, 4, "likes",
				LocalDate.now().minusMonths(1).toString());
		playlistService.generateAleatoryEditablePlayList(aleatoryFilters, admin.getUserId());
		orderSongDtoList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		assertEquals(4, orderSongDtoList.size());
		assertEquals(song1.getTitle(), orderSongDtoList.get(0).getTitle());
		assertEquals(song2.getTitle(), orderSongDtoList.get(1).getTitle());
		assertEquals(song3.getTitle(), orderSongDtoList.get(2).getTitle());
		assertEquals(song4.getTitle(), orderSongDtoList.get(3).getTitle());
	}

	@Test
	public void createPlaylistTest() {
		List<OrderSongPlDto> orderSongDtoList = new ArrayList<>();
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song7.getSongId());
		songIdList.add(song8.getSongId());
		songIdList.add(song6.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());

		Playlist playlist = playlistService.createPlayList("playlist1", "musica variada", admin.getUserId());
		assertTrue(playlistRepository.existsById(playlist.getPlaylistId()));
		orderSongDtoList = playlistService.getOrderSongDtoEditablePlaylistFromUser(admin.getUserId());
		// comprobamos que queda sin canciones editableplaylist
		assertEquals(0, orderSongDtoList.size());
	}

	@Test
	public void removePlaylistTest() {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song7.getSongId());
		songIdList.add(song8.getSongId());
		songIdList.add(song6.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		Playlist playlist = playlistService.createPlayList("playlist1", "musica variada", admin.getUserId());
		assertTrue(playlistRepository.existsById(playlist.getPlaylistId()));
		assertTrue(playlistService.removePlayList(playlist.getPlaylistId()));
		assertFalse(playlistRepository.existsById(playlist.getPlaylistId()));
	}

	@Test
	public void getDurationEditablePlaylistHoursMinSecondsTest() {
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song1.getSongId());
		songIdList.add(song2.getSongId());
		songIdList.add(song4.getSongId());
		// las canciones duran 6 minutos y 10 segundos en total
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		String result = playlistService.getDurationEditablePlaylistHoursMinSeconds(admin.getUserId());
		assertEquals("00:06:10", result);
	}

	@Test
	public void getAllPlaylistDtoTest() {
		// creamos 2 playlists
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song7.getSongId());
		songIdList.add(song8.getSongId());
		songIdList.add(song6.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("playlist1", "musica variada", admin.getUserId());

		songIdList = new ArrayList<>();
		songIdList.add(song8.getSongId());
		songIdList.add(song3.getSongId());
		songIdList.add(song1.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		playlistService.createPlayList("playlist2", "verano", admin.getUserId());

		List<PlaylistDto> resultList = playlistService.getAllPlaylistDto();

		assertEquals(2, resultList.size());
	}

	@Test
	public void getAllPlaylistsOrderByName() {
		// creamos 2 playlists
		List<Long> songIdList = new ArrayList<>();
		songIdList.add(song7.getSongId());
		songIdList.add(song8.getSongId());
		songIdList.add(song6.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		Playlist playlist1 = playlistService.createPlayList("lunes", "musica variada", admin.getUserId());

		songIdList = new ArrayList<>();
		songIdList.add(song8.getSongId());
		songIdList.add(song3.getSongId());
		songIdList.add(song1.getSongId());
		playlistService.addSongsEditablePlaylist(songIdList, admin.getUserId());
		Playlist playlist2 = playlistService.createPlayList("miercoles", "verano", admin.getUserId());

		List<Playlist> resultList = playlistService.getAllPlaylistsOrderByName();
		assertEquals(playlist1, resultList.get(0));
		assertEquals(playlist2, resultList.get(1));
	}

}
