package es.udc.fic.tfg.jp.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.tfg.jp.Application;
import es.udc.fic.tfg.jp.domain.user.User;
import es.udc.fic.tfg.jp.service.user.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@Test
	public void registerUserTest() {
		User user = userService.registerUser(new User("udc@udc.es", "pass", "user2", "udc"));

		User userResult = userService.findByEmail(user.getEmail());

		assertEquals(userResult, user);

	}

}
